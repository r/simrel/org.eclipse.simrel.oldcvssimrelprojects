#!/usr/bin/env bash

source checkForErrorExit.sh

# overall location for projects and tools
BUILD_HOME=${BUILD_HOME:-~/cvs2gitmigration/org.eclipse.simrel.cvs2gitmigration}

# location for a specific project or repository work
MIGR_HOME=${MIGR_HOME:-$BUILD_HOME/simrelmigration}

printf "\n\t%s\t%s" "BUILD_HOME:" $BUILD_HOME
printf "\n\t%s\t%s\n" "MIGR_HOME: " $MIGR_HOME

CVSROOT=${CVSROOT:-/cvsroot}
CVSPROJECT=${CVSPROJECT:-callisto}

printf "\n\t%s\t%s" "CVSROOT:" $CVSROOT
printf "\n\t%s\t%s\n" "CVSPROJECT: " $CVSPROJECT

MODULE_NAME=${MODULE_NAME:-}

printf "\n\t%s\n" "Processing Module: $MODULE_NAME"

# CVS_LOC should be the "real" (initial) cvs location
# which we copy to new location for processing before 
# migrating to git
#CVS_LOC=$CVSROOT/$CVSPROJECT/$MODULE_NAME
CVS_LOC=$CVSROOT/$CVSPROJECT

printf "\n\t%s\t%s\n" "CVS_LOC:" "$CVS_LOC"

MIGR_CVS_LOC=$MIGR_HOME/tempcvs

cd $BUILD_HOME
checkForErrorExit $? "change directory failed"

./clean.sh
checkForErrorExit $? "clean.sh failed"

mkdir -p $MIGR_CVS_LOC
checkForErrorExit $? "Could not make directory"

cd $MIGR_CVS_LOC
checkForErrorExit $? "Could not make directory"

cvs -d :local:$(pwd) init
checkForErrorExit $? "Could not cvs init new temp repo"

# Here we make a temp copy of origional cvs repo, where we could make "tweaks" 
# without effecting original
rsync -vr  $CVS_LOC/* $MIGR_CVS_LOC/
# no error check here, since there is a file in cvsroot we can not read, so would returns error
# even though all else is copied
#checkForErrorExit $? "rsync of original cvs location to cvs temp location failed"

# list links in cvs repo (and remove if found)
printf "\n\t%s\n" "Links in repo, if any:"
find $MIGR_CVS_LOC -type l 

# clean the CVS repo of links (TODO: need to rework if needed)
#printf "\n\t%s\n" "Remove Links in repo, if any:"
#find $MIGR_CVS_LOC -type l|xargs -n 1 rm

# generate the import data
# make sure the author names are correct
# make sure your options file points to the correct tmp cvsroot
cd $BUILD_HOME

SCRIPT_OPTIONS=$MIGR_HOME/scripts/cvs2git-${CVSPROJECT}.options

$BUILD_HOME/cvs2svn-trunk/cvs2git --options=$SCRIPT_OPTIONS 2>&1 | tee cvs2git_log.txt 
checkForErrorExit $? "cvs2git failed"

# create a git repo to continue the conversion
cd $BUILD_HOME
git init target.$CVSPROJECT
checkForErrorExit $? "git init failed"
cd target.$CVSPROJECT
checkForErrorExit $? "change directory failed"

cat $BUILD_HOME/cvs2svn-tmp/git-blob.dat $BUILD_HOME/cvs2svn-tmp/git-dump.dat | git fast-import
checkForErrorExit $? "git fastimport failed"

python $BUILD_HOME/cvs2svn-trunk/contrib/git-move-refs.py
checkForErrorExit $? "git-move-refs failed"

git prune
checkForErrorExit $? "git prune failed"
git repack -a -d --depth=250 --window=250
checkForErrorExit $? "git repack failed"
git gc --aggressive
checkForErrorExit $? "git gc failed"
git repack -a -d --depth=250 --window=250
checkForErrorExit $? "git repack failed"

cd $BUILD_HOME/target.$CVSPROJECT
checkForErrorExit $? "change directory failed"

git reset --hard
checkForErrorExit $? "git reset failed"

#create deploy
cd $BUILD_HOME
checkForErrorExit $? "change directory failed"

git clone --bare  target.$CVSPROJECT deploy.$CVSPROJECT
checkForErrorExit $? "git clone failed"

cd deploy.$CVSPROJECT
checkForErrorExit $? "change directory failed"

git remote rm origin
checkForErrorExit $? "git remote failed"
git config -l
checkForErrorExit $? "git config failed"
git config --add core.sharedrepository 1
checkForErrorExit $? "git config failed"

#final pack and prune step
git prune
checkForErrorExit $? "git prune failed"
git repack -a -d --depth=250 --window=250
checkForErrorExit $? "git repack failed"
git gc --aggressive
checkForErrorExit $? "git gc failed"
git repack -a -d --depth=250 --window=250
checkForErrorExit $? "git repack failed"

cd $BUILD_HOME
checkForErrorExit $? "change directory failed"

# for now, during development, remove remote repo we are recreating, 
# but probably should not leave this "auto deletion" in final production script
#rm -fr /gitroot/simrel/org.eclipse.simrel.oldcvssimrelprojects.git



#mkdir -p /gitroot/simrel
#checkForErrorExit $? "ssh mkdir directory failed"
# rememmber, do not use -p here, so will fail if already exists
mkdir /gitroot/simrel/org.eclipse.simrel.oldcvssimrelprojects.git
checkForErrorExit $? "mkdir directory failed"

cp -r deploy.$CVSPROJECT/*  /gitroot/simrel/org.eclipse.simrel.oldcvssimrelprojects.git/
checkForErrorExit $? "cp deploy failed"

printf "\n\t%s\t%s\n"  "INFO:" "DONE, $CVSPROJECT"

