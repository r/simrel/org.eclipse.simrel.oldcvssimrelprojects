#!/usr/bin/env bash

# overall location for projects and tools
export BUILD_HOME=${BUILD_HOME:-~/cvs2gitmigration/org.eclipse.simrel.cvs2gitmigration}

# location for a specific project or repository work
#export MIGR_HOME=${MIGR_HOME:-$BUILD_HOME/simrelmigration}

printf "\n\t%s\t%s" "BUILD_HOME:" $BUILD_HOME
#printf "\n\t%s\t%s\n" "MIGR_HOME: " $MIGR_HOME

#get the tool, once
# assumes svn is installed on this system

cd $BUILD_HOME
if [ -d cvs2svn-trunk ]
then
    printf "\n\t%s\t%s\n" "INFO: " "cvs2svn-trunk directory already exists, assuing tools already installed"
else
    svn co --username=guest --password="" http://cvs2svn.tigris.org/svn/cvs2svn/trunk cvs2svn-trunk
fi 

export CVS2GITEXE=$BUILD_HOME/cvs2svn-trunk/cvs2git

printf "\n\t%s\t%s\n" "CVS2GITEXE:" $CVS2GITEXE


