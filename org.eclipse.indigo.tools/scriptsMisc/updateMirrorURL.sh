#!/usr/bin/env bash
# script to copy update jars from their working area to the staging area

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source indigo_properties.shsource


directoryToChange=$1
newMirrorURL=$2

# make sure 'directoryToChange' has been defined and is no zero length
if [ -z "${directoryToChange}" ]
then
   echo "The variable directoryToChange must be defined to run this script."
   echo "    (Normally first argument)."
   exit 1;
fi

# make sure 'newMirrorURL' has been defined and is no zero length
if [ -z "${newMirrorURL}" ]
then
   echo "The variable newMirrorURL must be defined to run this script."
   echo "    (Normally second argument)."
   exit 1;
fi

workingDirectory=${HOME}/temp/testdir

mkdir -p "${workingDirectory}"

fromFile=artifacts.jar
workFile=artifacts.xml

# make sure file to change exists
if [ ! -e "${directoryToChange}/${fromFile}" ]
then
   echo "The expected file does not exist?"
   exit 2;
fi


echo ""
echo "    Fixup URL "
echo "        original: ${directoryToChange}/${fromFile}"
echo "         working: ${workingDirectory}/${workFile}"
echo ""

echo "copy ${fromFile} to working area" 
rsync -vp ${directoryToChange}/${fromFile} ${workingDirectory}

echo "unzip the jar, and make a backup of it for later comparison"
unzip -o ${workingDirectory}/${fromFile} -d ${workingDirectory}
cp ${workingDirectory}/${workFile} ${workingDirectory}/${workFile}.bak

#TODO: I'm sure there is a way to get string eval's correct, 
#      but escaped parens are needed in one place, but not another.
echo "make sure there is only one occurance that will be changed in file"
matchingPhrase="<property name='p2\.mirrorsURL'\(.*\)file=.*&amp;protocol=http'/>"
fromString="<property name='p2\.mirrorsURL'(.*)file=.*&amp;protocol=http'/>"
echo "matchingPhrase: "${matchingPhrase}
echo "fromString: "${fromString}

matchedLines=`grep "^ *${matchingPhrase} *$" "${workingDirectory}"/"$workFile"`
nMatches=`echo $matchedLines | wc -l`
echo
echo "    matches before replacement: "${nMatches}
echo "    matchedLine(s) before replacement: "
echo "        "$matchedLines
echo

if [[ $nMatches == 1 ]]
then

    

    # compose toString and replacement command
    toString="<property name='p2\.mirrorsURL'\$2file="$newMirrorURL"&amp;protocol=http'/>"
    
    replaceCommand="s!^( *)${fromString} *\$!\$1${toString}!g"
    echo "    replaceCommand: ${replaceCommand}"

    echo "    use perl to do replacement"
    perl -pi -w -e "${replaceCommand}" "${workingDirectory}"/"$workFile"

    perlResult=$?
    if [[ $perlResult -ne 0 ]] 
    then 
    	echo "Perl return code was non-zero, ${perlResult}, so exiting and not changing anything"
    	exit $perlResult
    fi
    
       matchedLines=`grep "^ *${matchingPhrase} *$" "${workingDirectory}"/"$workFile"`
       nMatches=`echo $matchedLines | wc -l`
       echo
       echo "    matches after replacement: "${nMatches}
       echo "    matchedLine(s) after replacement: "
       echo "        "$matchedLines
       echo

else
    echo " Error: number of matches to original ("$nMatches") before replacment was not as expected, so exiting and not changing anything"
    exit 3
fi
              
echo "rezip our modified version"
zip -Dj ${workingDirectory}/${fromFile} ${workingDirectory}/${workFile}

#todo: would this be helpful?
#echo "set file times to match original"
#touch ${workingDirectory}/${fromFile} --reference=${directoryToChange}/${fromFile}

echo "create backup of original"
cp ${directoryToChange}/${fromFile} ${directoryToChange}/${fromFile}.bak
echo "copy new over old"
cp ${workingDirectory}/${fromFile} ${directoryToChange}/



