#!/usr/bin/env bash


# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source indigo_properties.shsource
 
echo "   removing all of previous project ..."
rm -fr "${BUILD_MODEL_DIR}"/*
mkdir -p "${BUILD_MODEL_DIR}"

buildBranch=HEAD

cd ${BUILD_HOME}
echo "   checking out branch of project from cvs ..."
cvs -d :pserver:anonymous@dev.eclipse.org:/cvsroot/callisto  export -d org.eclipse.indigo.build -r $buildBranch org.eclipse.indigo.build

