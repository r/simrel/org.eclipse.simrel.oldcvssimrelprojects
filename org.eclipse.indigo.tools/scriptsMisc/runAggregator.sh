#!/usr/bin/env bash

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source indigo_properties.shsource

echo "BUILD_HOME: " ${BUILD_HOME}
export BUILD_TIMESTAMP=$(date --utc +%Y%m%d%H%M%S); 
echo "BUILD_TIMESTAMP: " $BUILD_TIMESTAMP
echo "BUILD_RESULTS: " $BUILD_RESULTS

export BUILD_ID=${BUILD_TIMESTAMP}

OTHER_ARGS="--mockEmailTo david_williams@us.ibm.com --buildModel "${BUILD_MODEL_DIR}"/indigo.b3aggr --buildRoot ${BUILD_RESULTS} --buildId ${BUILD_ID} --action CLEAN_BUILD --packedStrategy UNPACK_AS_SIBLING --mavenResult --logLevel DEBUG"

VM_ARGS="-vmargs -Declipse.p2.mirrors=false -Xmx256m"
# -Declipse.p2.MD5Check=false

APP_NAME=org.eclipse.b3.cli.headless

echo "ECLIPSE_HOME: " ${ECLIPSE_HOME}
EQLAUNCHER=`find ${ECLIPSE_HOME}/plugins -name org.eclipse.equinox.launcher_*.jar`
echo "EQLAUNCHER: " $EQLAUNCHER


"${JAVA_CMD}" -jar ${EQLAUNCHER} -vm "${JAVA_CMD}" -console  -data ${BUILD_HOME}/${BUILD_TIMESTAMP}/workspace-runAggregator -debug -nosplash -application ${APP_NAME} aggregate ${OTHER_ARGS} ${VM_ARGS}


