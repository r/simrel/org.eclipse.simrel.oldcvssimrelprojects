#!/usr/bin/env bash
# script to copy update jars from their working area to the staging area

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source indigo_properties.shsource
  
fromDirectory=${stagingDirectory}

toDirectory=${HOME}/temp/testdir

mkdir -p "${toDirectory}"

fromFile=compositeArtifacts.jar
workFile=compositeArtifacts.xml


echo ""
echo "    Fixup URL "
echo "        original: ${fromDirectory}/${fromFile}"
echo "         working: ${toDirectory}/${workFile}"
echo ""

echo "copy compositeArtifacts.jar to working area" 
rsync -vp ${fromDirectory}/${fromFile} ${toDirectory}

echo "unzip the jar, and make a backup of it for later comparison"
unzip -o ${toDirectory}/${fromFile} -d ${toDirectory}
cp ${toDirectory}/${workFile} ${toDirectory}/${workFile}.bak

echo "make sure there is only one occurance that will be changed in file"
matchingPhrase="<child location='http://download\.eclipse\.org/eclipse/updates/3\.5milestones/I20090611-1540/'/>"
matchedLines=`grep "^ *${matchingPhrase} *$" "${tempDir}"/"$workFile"`
nMatches=`echo $matchedLines | wc -l`
echo
echo "    matches before replacement: "${nMatches}
echo "    matchedLine(s) before replacement: "
echo "        "$matchedLines
echo

if [[ $nMatches == 1 ]]
then

    fromString="${matchingPhrase}"

    # compose toString and replacement command
    toString="<child location='http://download.eclipse.org/eclipse/updates/3.5/'/>"
    
    replaceCommand="s!^( *)${fromString} *\$!\$1${toString}!g"
    echo "    replaceCommand: ${replaceCommand}"

    echo "    use perl to do replacement"
    perl -pi -w -e "${replaceCommand}" "${toDirectory}"/"$workFile"

    perlResult=$?
    if [[ $perlResult -ne 0 ]] 
    then 
    	echo "Perl return code was non-zero, ${perlResult}, so exiting and not changing anything"
    	exit $perlResult
    fi

    echo "make sure no matches to original remaining"
    nOriginalMatches=`grep "^ *${matchingPhrase} *$" "${toDirectory}"/"$workFile" | wc -l`
    echo
    echo "   matchedLine(s) after replacement: " $nOriginalMatches " (should be zero)"
    echo
    if [[ $nOriginalMatches -ne 0 ]]
    then
        echo " Error: number of matches to original phrase (" $nOriginalMatches ") after replacement was greater than expected (should be zero), so exiting and not changing anything"
        exit 1
    fi
    
    echo "make sure there is exactly one match for new string"
    resultingPhrase="${toString}"
    changedLines=`grep  "^ *${resultingPhrase} *$" "${toDirectory}"/"$workFile"`
    nChangedLines=`echo $changedLines  | wc -l`
    echo
    echo "   Number of lines changed: " $nChangedLines
    echo "   Changeded lines: " 
    echo "      "$changedLines
    echo
    
    if [[ $nChangedLines -ne 1 ]]
    then
        echo " Error: number of matches to expected ("$nChangedLines") after replacement was not as expected (should be zero), so exiting and not changing anything"
        exit 1
    fi
else
    echo " Error: number of matches to original ("$nMatches") before replacment was not as expected, so exiting and not changing anything"
    exit 1
fi
              
echo "rezip our modified version"
zip -Dj ${toDirectory}/${fromFile} ${toDirectory}/${workFile}
#echo "set file times to match original"
#touch ${toDirectory}/${fromFile} --reference=${fromDirectory}/${fromFile}
echo "create backup of original"
cp ${fromDirectory}/${fromFile} ${fromDirectory}/${fromFile}.bak
echo "copy new over old"
cp ${toDirectory}/${fromFile} ${fromDirectory}/



