#!/usr/bin/env bash

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source indigo_properties.shsource

ECLIPSE_HOME=${ECLIPSE_HOME_36}/eclipse

echo JAVA_EXEC_DIR: $JAVA_EXEC_DIR;
echo ECLIPSE_HOME: $ECLIPSE_HOME; 

APP_NAME=org.eclipse.equinox.p2.director
REPO_LOCATION="http://download.eclipse.org/modeling/emft/b3/headless-3.7milestones/"
REPO_ARGS="-metadataRepository ${REPO_LOCATION} -artifactRepository ${REPO_LOCATION}"
IU_ARGS="-installIU org.eclipse.b3.aggregator.engine.feature.feature.group"

echo APP_NAME: ${APP_NAME}
echo REPO_LOCATION: ${REPO_LOCATION}
echo REPO_ARGS: ${REPO_ARGS}
echo IU_ARGS: ${IU_ARGS}

VM_ARGS="-vmargs -Xmx512m -Declipse.p2.mirrors=false"
echo VMARGS: ${VM_ARGS}

"${ECLIPSE_HOME}"/eclipse -consolelog -data ./workspace-getAggregator -debug -nosplash -vm "${JAVA_EXEC_DIR}" -application ${APP_NAME} ${REPO_ARGS} ${IU_ARGS} ${VM_ARGS}


