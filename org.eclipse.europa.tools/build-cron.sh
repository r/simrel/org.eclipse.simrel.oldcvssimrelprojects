#!/bin/sh

cd /home/data/users/bfreeman/europa/
./getEuropaToolsFromCVS.sh 2>&1 | tee /home/data/users/bfreeman/europa/out.txt
cd /home/data/users/bfreeman/europa/org.eclipse.europa.tools/build-home/
./runUpdateAllProjects.sh -clean 2>&1 | tee --append /home/data/users/bfreeman/europa/out.txt