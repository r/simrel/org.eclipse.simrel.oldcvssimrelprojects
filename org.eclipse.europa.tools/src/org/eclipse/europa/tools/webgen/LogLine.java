package org.eclipse.europa.tools.webgen;

import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LogLine {
	public String line;

	public int linenumber;

	public Status status;

	public String anchor;

	public String anchor2;

	private static int nextanchor = 1;
//	private static Pattern p = Pattern
	//.compile("(.*Command-line arguments:.*\\-from: )([^ ]+)( .*)");
	private static Pattern p1 = Pattern
	.compile("(.*Command-line arguments:.*-from file:///home/data/users/bfreeman/)(\\S+)(.*)");
	private static Pattern p2 = Pattern.compile("(.*Command-line arguments:.*-from )(http://download.eclipse.org/\\S+)(.*)");
	
	public LogLine(String s, int number) {
		this.linenumber = number;
		this.line = s;
		this.status = Status.unknown;
	}

	public void addAnchor() {
		anchor = "" + nextanchor++;
	}

	public void saveAnchor() {
		this.anchor2 = this.anchor;
		this.anchor = null;
	}

	public void write_log_line(PrintWriter writer) {
		if (status.isFail()) {
			writer.print("<span style=\"background-color: "
					+ WebPageGenerator.fail_color() + ";\">");
		}
		if (status.isSuccess()) {
			writer.print("<span style=\"background-color: "
					+ WebPageGenerator.success_color() + ";\">");
		}
		writer.print("<code>");
		if (anchor != null) {
			writer.print("<a name=\"" + anchor + "\"></a>");
		}
		Matcher m1 = p1.matcher(line);
		Matcher m2 = p2.matcher(line);
		if( m1.matches() ) {
			writer.print(m1.group(1));
			writer.print("<span style=\"background-color: "
					+ WebPageGenerator.information_color() + ";\">");
			writer.print(m1.group(2));
			writer.print("</span>");
			writer.print(m1.group(3));
		} else if( m2.matches() ) {
				writer.print(m2.group(1));
				writer.print("<span style=\"background-color: "
						+ WebPageGenerator.information_color() + ";\">");
				writer.print(m2.group(2));
				writer.print("</span>");
				writer.print(m2.group(3));
		} else {
			writer.print(line);
		}
		writer.print("</code>");
		if (status.isFail() || status.isSuccess() || status.isInformation()) {
			writer.print("</span>");
		}
	}
}
