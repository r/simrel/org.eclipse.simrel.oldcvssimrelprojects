package org.eclipse.europa.tools.webgen;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import org.xml.sax.SAXException;

public class LogFileToWebPages {
	public static void main(String[] argv) throws IOException, SAXException {
		System.out.println("Generating Europa build status pages");

		File f = new File(argv[0]);
		File d = new File(argv[1]);
		File s = new File(argv[4]);
		LogFileParser parser = new LogFileParser(f);
		parser.parse();
		System.out.println("parsing complete");

		String feature = argv[2];
		String sitefilename = argv[3] + "/site.xml";
		Map<String, Map<String, Object>> results = new Hashtable<String, Map<String, Object>>();
		(new CheckForFeatureInSiteXml()).process(new File(feature), new File(
				sitefilename), results);
		System.out.println("site-features comparison complete");

		for (Iterator<ProjectLog> iterator = parser.projects.iterator(); iterator
				.hasNext();) {
			ProjectLog project = iterator.next();
			if (project.project.equals("pack200")) {
				project.scanForPack200Status(parser.projects);
			}
		}
		System.out.println("pack200 status check complete");

		for (Iterator<ProjectLog> iterator = parser.projects.iterator(); iterator
				.hasNext();) {
			ProjectLog project = iterator.next();
			if (project.project.equals("scanForLicenses")) {
				project.annotate_features_with_license_information(parser.projects);
			}
		}
		System.out.println("license check complete");

		Date d1 = new Date(s.lastModified());
		Date d2 = new Date(f.lastModified());
		String bd = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG).format(d1);
		String sd = DateFormat.getDateTimeInstance(DateFormat.LONG,
				DateFormat.LONG).format(d2);
		WebPageGenerator generator = new WebPageGenerator(parser, d, bd, sd,
				results);
		generator.generate();
		System.out.println("web pages complete");

		System.out.println("Generation complete.");
	}
}
