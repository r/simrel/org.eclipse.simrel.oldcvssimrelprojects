package org.eclipse.europa.tools.webgen;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.text.DateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.SAXException;

public class UnderConstructionWebPages {
	public static void main(String[] argv) throws IOException, SAXException {
		System.out
				.println("Marking Europa build status pages as under construction");

		File d = new File(argv[0]);
		File f = new File(d, "index.html");
		Reader r = new FileReader(f);
		int len = (int) f.length();
		char[] buf = new char[len];
		r.read(buf);
		String s = new String(buf);
		r.close();

		if (!s.contains("New Build in Progress")) {
			Date d1 = new Date();
			String ds = DateFormat.getDateTimeInstance(DateFormat.LONG,
					DateFormat.LONG).format(d1);
			Pattern p = Pattern.compile("\\<h1");
			Matcher m = p.matcher(s);
			s = m
					.replaceFirst("<p><em><font size=-1>New build started " + ds + "</font></em></p><h1 style=\"background-color: #FFFFDF;\">New Build in Progress - these results are from the previous build</h1><h1");
			p = Pattern.compile("\\<head\\>");
			m = p.matcher(s);
			s = m.replaceFirst("<head><meta http-equiv=\"refresh\" content=\"600\">" );
			Writer w = new FileWriter(f);
			w.write(s);
			w.close();

			System.out.println("Marking complete.");
		} else {
			System.out.println("Already marked; no change");
		}
	}
}
