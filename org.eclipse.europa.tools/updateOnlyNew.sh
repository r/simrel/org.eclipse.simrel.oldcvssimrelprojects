
if [ "$1" != "-doit" ] ; then
   echo "    This is a dry run. Add -doit to actually remove"
   doit="--dry-run"
else
   doit=""
fi

rsync $doit  --ignore-existing -rv ~/downloads/releases/europa/staging/plugins ~/downloads/releases/europa

rsync $doit  --ignore-existing -rv ~/downloads/releases/europa/staging/features ~/downloads/releases/europa

if [ "$doit" = "--dry-run" ] ; then
   echo "    This was a dry run. Add -doit to actually remove"
fi

