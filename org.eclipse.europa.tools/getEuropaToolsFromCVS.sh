
# This script should be "copied up" and ran from a
# directory "above" the main working directories of
# org.eclipse.europa.updatesite, and
# org.eclipse.europa.tools
# this is typically the users $HOME directory

echo "   "
echo "   Removing old  org.eclipse.europa.* and exporting fresh HEAD versions." 
echo "   "

echo "        Removing org.eclipse.europa.updatesite."
rm -fr org.eclipse.europa.updatesite
echo "        Removing org.eclipse.europa.tools." 
echo "   "
rm -fr org.eclipse.europa.tools

# ISSUE: we should move away from using 'head' version
cvs -Q -f -d /cvsroot/callisto export -r HEAD org.eclipse.europa.updatesite
cvs -Q -f -d /cvsroot/callisto export -r HEAD org.eclipse.europa.tools

chmod -R +x org.eclipse.europa.tools/*.sh
chmod -R +x org.eclipse.europa.tools/**/*.sh

#echo "    "
#echo "    building log parser tools"
#echo "    "
#pushd org.eclipse.europa.tools/build-home > /dev/null
#ant -f buildwebgen.xml compile
#popd > /dev/null

echo " " 
echo "    for complete refresh, use something similar to following"
echo " " 
echo "    cd org.eclipse.europa.tools/build-home/"
echo "    screen -dmS europa"
echo "    ./runUpdateAllProjects.sh -clean 2>&1 | tee ~/europa/out.txt"
echo " "