#!/bin/sh

# OSGi Indexer. See
# http://www2.osgi.org/Repository/HomePage
# and 
# http://www2.osgi.org/Repository/BIndex

# note: the bindex.jar must be obtained from OSGi, it
# is not part of Eclipse

# must be ran literally from root of repository ... else file paths all off.
# todo: fix this issue with paths, so can be ran from ~/

java -jar ~/osgiIndexer/bindex.jar -name Eclipse -l http://www.eclipse.org/org/documents/epl-v10.php  -t http://download.eclipse.org/europa/releases/plugins/%f -q plugins/*.jar -r repository.zip

