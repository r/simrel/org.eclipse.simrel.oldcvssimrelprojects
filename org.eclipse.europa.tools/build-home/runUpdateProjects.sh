#!/bin/sh

# script to run update, setting properties, for an individual project

# output to "local" machine's home, especially for this scripts purpose
# of testing just one project
updateSite=$HOME/europa/staging
 
 
# Include machine-specifc properties from source include. 
# The 'source' command can have complicated "lookup" rules, 
# that can be effected by various settings (see man pages) 
# but in typical cases will find the first file on the users 
# path, and if not there, then find the one in the currect 
# directory. Hence, users, on non-standard machines can keep 
# machine specific settings there and not have to always "hand edit" 
# new copies of the script files. If the file is not found, due 
# to some odd settings, then the script will simply fail, and 
# it's back to hand editing.  
source europaProperties.shsource

# check that the user specified an argument
project="$1"
if [ "$project" !=  "" ] 
then 
  	
     ant  -f features-$project.xml -Declipse.home=$eclipseLocation -DjvmLocation=$jvmLocation -DlocalUpdateSitePath=$updateSite -Dosgi.arch=ppc -Dosgi.clean -Dosgi.debug=true -Declipse.consoleLog=true -DcontinueOnError=true


else 
     echo "   Usage: $0 <project> (where <project> is the xxx suffix of feature-xxx file, such as ep, wtp, birt, etc.)"

fi
