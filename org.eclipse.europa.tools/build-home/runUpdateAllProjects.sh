#!/bin/sh

# script to run update, setting properties, for all projects


ulimit -n 2014 
export JAVA_HIGH_ZIPFDS=500 

updateSite=$HOME/europa/staging
downloadSite=$HOME/downloads/releases/europa/staging
 
# Include machine-specifc properties from source include. 
# The 'source' command can have complicated "lookup" rules, 
# that can be effected by various settings (see man pages) 
# but in typical cases will find the first file on the users 
# path, and if not there, then find the one in the currect 
# directory. Hence, users, on non-standard machines can keep 
# machine specific settings there and not have to always "hand edit" 
# new copies of the script files. If the file is not found, due 
# to some odd settings, then the script will simply fail, and 
# it's back to hand editing.  
source europaProperties.shsource

#
# Record the start-of-build time
#
touch ../../out.web/start.time

#
# Mark the current status web pages as "under construction"
#
ant -f buildwebgen.xml under.construction
scp -r ../../out.web/index.html dash.eclipse.org:public_html/europa/


if [ "$1" == "-clean" ] ; then

	 echo "   Removing $updateSite/features/, plugins/, and root files"
	 mkdir $updateSite/
	 rm -fr $updateSite/features/
	 rm -fr $updateSite/plugins/
	 rm -fr $updateSite/*
	 echo "   Updating index and web site files, but not site xml files (yet)"
	    cp $HOME/europa/org.eclipse.europa.updatesite/WebContent/index.html  $updateSite
	    cp $HOME/europa/org.eclipse.europa.updatesite/WebContent/*.jpg  $updateSite 
	    cp $HOME/europa/org.eclipse.europa.updatesite/WebContent/*.gif  $updateSite 
	    cp -r $HOME/europa/org.eclipse.europa.updatesite/WebContent/web $updateSite 

fi


ant  -f updateMirrorAll.xml -Declipse.home=$eclipseLocation -DjvmLocation=$jvmLocation -DlocalUpdateSitePath=$updateSite

#
# merge authored site.xml files with the most recent auto generated one
#
echo '[echo] -------- Project: update site.xml Responsible: bfreeman'
$HOME/europa/org.eclipse.europa.tools/build-home/runUpdateSiteXmlFiles.sh
echo 'BUILD SUCCESSFUL'

#ant  -f scanForLicenses.xml -DupdateSite=$updateSite

ant  -f createPack200s.xml     -Declipse.home=$eclipseLocation -DjvmLocation=$jvmLocation -DupdateSite=$updateSite

# be sure digest created after the zipped jars
ant  -f createDigests.xml      -Declipse.home=$eclipseLocation -DjvmLocation=$jvmLocation -DupdateSite=$updateSite

ant -f tempFix187396.xml -DupdateSite=$updateSite -DdownloadSite=$downloadSite

ant -f copyToDownload.xml -DupdateSite=$updateSite -DdownloadSite=$downloadSite

#
# Generate the status web pages
#
ant -f buildwebgen.xml 
scp -r ../../out.web/* dash.eclipse.org:public_html/europa/

