#!/bin/sh

# script to run update, setting properties, for all projects


ulimit -n 2014 
export JAVA_HIGH_ZIPFDS=500 

updateSite=$HOME/europa/staging
downloadSite=$HOME/downloads/releases/europa/staging
 
# Include machine-specifc properties from source include. 
# The 'source' command can have complicated "lookup" rules, 
# that can be effected by various settings (see man pages) 
# but in typical cases will find the first file on the users 
# path, and if not there, then find the one in the currect 
# directory. Hence, users, on non-standard machines can keep 
# machine specific settings there and not have to always "hand edit" 
# new copies of the script files. If the file is not found, due 
# to some odd settings, then the script will simply fail, and 
# it's back to hand editing.  
source europaProperties.shsource


echo '[echo] -------- Project: update site.xml Responsible: bfreeman'
$HOME/europa/org.eclipse.europa.tools/build-home/runUpdateSiteXmlFiles.sh
echo 'BUILD SUCCESSFUL'

#ant  -f createPack200s.xml     -Declipse.home=$eclipseLocation -DjvmLocation=$jvmLocation -DupdateSite=$updateSite

# be sure digest created after the zipped jars
#ant  -f createDigests.xml      -Declipse.home=$eclipseLocation -DjvmLocation=$jvmLocation -DupdateSite=$updateSite

ant -f tempFix187396.xml -DupdateSite=$updateSite -DdownloadSite=$downloadSite

#ant -f copyToDownload.xml -DupdateSite=$updateSite -DdownloadSite=$downloadSite
