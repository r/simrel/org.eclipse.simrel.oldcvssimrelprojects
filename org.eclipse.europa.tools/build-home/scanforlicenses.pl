#/usr/bin/perl

use strict;
use File::Path;

my $updateSite = shift;

my $text;

my %plugsin2version;
open INP, "plugins_with_correct_legal_docs.txt" or die $!;
while( <INP> ) {
	next if $_ =~ /^#/;
	$_ =~ /(\S+)\s+(\S+)/;
	$plugsin2version{$1}{$2} = 1;
}
close INP;

my $tempdir = '/tmp/europamatic1/';
		rmtree( $tempdir, 0, 1 );

my %plugin2feature;

my $featuredir = $updateSite . '/features';
opendir(DIR, $featuredir);
my @files = readdir(DIR);
closedir(DIR);

foreach my $file (@files) {
	if( -d $file ) {
		# skip
	} elsif( $file =~ /.*.pack.gz/ ) {
		# skip
	} else {
		mkdir $tempdir;
		system( "cd $tempdir; jar xf ${featuredir}/$file" );
		checkfeature( $tempdir, $file );
		rmtree( $tempdir, 0, 1 );
	}
}

sub checkfeature {
	my $dir = shift;
	my $feature = shift;
	print "F $feature";

	my $license_file = -e "$dir/license.html";
	my $epl_file = -e "$dir/epl-v10.html";
	$feature =~ /(\S+)_([\d\.]+)\.(\S+?)\.jar/;
	my $cert = 0;
	$cert = 1 if( $plugsin2version{$1}{$2} );
	
	if( $license_file
	 && $epl_file
	 ) {
	 	if( $cert ) {
	 		# everything is ok
	 	} else {
			print " has not been certified as having been reviewed";
		}
	} else {
		if( $cert ) {
			print " feature was certified as correct but it is missing: ";
		} else {
			print " feature is missing: ";
		}
		print "license.html " if( !$license_file );
		print "epl-v10.html " if( !$epl_file );
	}
	my $xml;
	open INP, "$dir/feature.xml";
	{
	    local( $/ ) ;
	    $xml = <INP>;
	}
	close INP;
	while( $xml =~ s/plugin\s+.*?id="([a-z0-9\.\_]+)"/XXX/ ) {
		$plugin2feature{$1} = $feature;
	}
	print "\n";
}

my $plugindir = $updateSite . '/plugins';
opendir(DIR, $plugindir);
my @files = readdir(DIR);
closedir(DIR);

foreach my $file (@files) {
	if( -d $file ) {
		if( $file != '.' && $file != '..' ) {
			checkplugin( $plugindir . $file, $file );
		}
	} elsif( $file =~ /.*.pack.gz/ ) {
		# skip
	} else {
		mkdir $tempdir;
		system( "cd $tempdir; jar xf ${plugindir}/$file" );
		checkplugin( $tempdir, $file );
		rmtree( $tempdir, 0, 1 );
	}
}

sub checkplugin {
	my $dir = shift;
	my $plugin = shift;
	print "P ";
	$plugin =~ /([a-z0-9\.]+)_\d/;
	my $pid = $1;
	if( $plugin2feature{$pid} ) {
		print $plugin2feature{$pid};
	} else {
		print "unknown";
	}
	print "   $plugin ";
	#system( "dir $dir" );
	if( -e "$dir/about.html" ) {
		$plugin =~ /(\S+)_([\d\.]+)\.(\S+?)\.jar/;
		if( $plugsin2version{$1}{$2} ) {
			# ok
		} else {
			print " has not been certified as having been reviewed";
		}
	} else {
		$plugin =~ /(\S+)_([\d\.]+)\.(\S+?)\.jar/;
		if( $plugsin2version{$1}{$2} ) {
			print " was certified as correct but about.html file is missing";
		} else {
			print " about.html file is missing from plug-in";
		}
	}
	print "\n";
}
