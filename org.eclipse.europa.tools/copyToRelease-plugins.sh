#!/bin/sh

# script to copy europa update jars from their staging to releases area

toDir=~/downloads/releases/europa
fromDir=~/downloads/releases/europa/staging

cp -f -p -r -v --update $fromDir/features --target-directory=$toDir/
cp -f -p -r -v --update $fromDir/plugins  --target-directory=$toDir/

