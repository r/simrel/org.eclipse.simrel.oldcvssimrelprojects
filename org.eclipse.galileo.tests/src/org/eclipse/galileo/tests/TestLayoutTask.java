package org.eclipse.galileo.tests;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class TestLayoutTask extends Task {

	private String directoryToCheck;
	private String tempWorkingDir;
	private boolean failuresOccurred = false;
	private boolean featureFailures = false;
	private boolean bundleFailures = false;

	@Override
	public void execute() throws BuildException {
		
		System.out.println("Starting BREE Rule Test");
		BREETest breeTest = new BREETest();
		breeTest.setDirectoryToCheck(getDirectoryToCheck());
		breeTest.setTempWorkingDir(getTempWorkingDir());
		bundleFailures = breeTest.testBREESettingRule();
		if (bundleFailures) {
			setFailuresOccurred(true);
		}
		
		System.out.println("Starting Version Form Test");
		Pack200Test packTest = new Pack200Test();
		packTest.setDirectoryToCheck(getDirectoryToCheck());
		bundleFailures = packTest.testBundlePack();
		if (bundleFailures) {
			setFailuresOccurred(true);
		}
		
		System.out.println("Starting Version Form Test");
		VersionTest versionTest = new VersionTest();
		versionTest.setDirectoryToCheck(getDirectoryToCheck());
		featureFailures = versionTest.testFeatureVersions();
		bundleFailures = versionTest.testBundleVersions();
		if (featureFailures || bundleFailures) {
			setFailuresOccurred(true);
		}
		
		System.out.println("Starting TestLayoutTask");
		TestLayoutTest test = new TestLayoutTest();
		test.setDirectoryToCheck(getDirectoryToCheck());
		test.setTempWorkingDir(getTempWorkingDir());
		featureFailures = test.testFeatureLayout();
		bundleFailures = test.testBundleLayout();
		if (featureFailures || bundleFailures) {
			setFailuresOccurred(true);
		}
	}

	public String getDirectoryToCheck() {
		return directoryToCheck;
	}

	public void setDirectoryToCheck(String bundleDirToCheck) {
		this.directoryToCheck = bundleDirToCheck;
	}

	public String getTempWorkingDir() {
		return tempWorkingDir;
	}

	public void setTempWorkingDir(String tempWorkingDir) {
		this.tempWorkingDir = tempWorkingDir;
	}

	public boolean isFailuresOccurred() {
		return failuresOccurred;
	}

	public void setFailuresOccurred(boolean failuresOccurred) {
		this.failuresOccurred = failuresOccurred;
	}

}
