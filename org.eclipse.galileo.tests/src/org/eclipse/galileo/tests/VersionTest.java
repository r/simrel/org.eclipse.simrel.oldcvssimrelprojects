/*******************************************************************************
 * Copyright (c) 2007, 2009 IBM Corporation and others. All rights reserved. This
 * program and the accompanying materials are made available under the terms of
 * the Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: IBM Corporation - initial API and implementation
 * This file originally came from 'Eclipse Orbit' project then adapted to use 
 * in WTP and improved to use 'Manifest' to read manifest.mf, instead of reading 
 * it as a properties file.
 ******************************************************************************/
package org.eclipse.galileo.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.tools.ant.BuildException;
import org.eclipse.internal.provisional.equinox.p2.jarprocessor.JarProcessor;
import org.eclipse.osgi.util.ManifestElement;
import org.osgi.framework.BundleException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @since 3.3
 */
public class VersionTest extends TestCase {

	private static final String EXTENSION_JAR = ".jar";
	private static final String EXTENSION_PACEKD_JAR = ".jar.pack.gz";
	private static final String EXTENSION_ZIP = ".zip";
	FullJarNameParser nameParser = new FullJarNameParser();
	private String BACKSLASH = "\\";
	private String LITERAL_PERIOD = BACKSLASH + ".";
	private String ANY = ".*";
	private Pattern threedots = Pattern.compile(ANY + LITERAL_PERIOD + ANY + LITERAL_PERIOD + ANY + LITERAL_PERIOD + ANY);

	public static void main(String[] args) {
		System.out.println("start TestLayoutTest main");
		VersionTest testlayout = new VersionTest();
		testlayout.setDirectoryToCheck("D:\\temptest");
		testlayout.testFeatureVersions();
		testlayout.testBundleVersions();

	}

	public static Test suite() {
		return new TestSuite(VersionTest.class);
	}

	private String directoryToCheck;


	public String getDirectoryToCheck() {
		return directoryToCheck;
	}

	public void setDirectoryToCheck(String bundleDirToCheck) {
		this.directoryToCheck = bundleDirToCheck;
	}

	public boolean testBundleVersions() {

		String property = getDirectoryToCheck();
		assertNotNull("Need to set input directory to check against.", property);
		String bundleDirectory = null;
		if (property.endsWith("/")) {
			bundleDirectory = property + "plugins";
		}
		else {
			bundleDirectory = property + "/plugins";
		}
		File inputdir = new File(bundleDirectory);
		assertTrue("bundle direcotry (" + bundleDirectory + ") must be an existing directory.", inputdir.exists() && inputdir.isDirectory());
		System.out.println(" Test Bundle Versions");
		boolean failuresOccured = checkFilesVersions(inputdir);
		return failuresOccured;
	}

	private boolean checkFilesVersions(File inputdir) {
		// reset/initialize errors
		List errors = new ArrayList();
		boolean failuresOccured = false;
		File[] children = inputdir.listFiles();
		int totalsize = children.length;
		int checked = 0;
		for (int i = 0; i < children.length; i++) {
			File child = children[i];
			String name = child.getName();
			// assume directory if not file
			if (child.isFile()) {
				if (name.endsWith(EXTENSION_PACEKD_JAR)) {
					name = getBasicName(name, EXTENSION_PACEKD_JAR);
				}
				else if (name.endsWith(EXTENSION_JAR)) {
					name = getBasicName(name, EXTENSION_JAR);
				}
				else if (name.endsWith(EXTENSION_ZIP)) {
					name = getBasicName(name, EXTENSION_ZIP);
				}
			}
			checked++;
			boolean valid = nameParser.parse(name);
			if (!valid) {
				errors.add(name + " does not have a valid version (it is unparsable)");
			}
			else {
				String version = nameParser.getVersionString();
				Matcher matcher = threedots.matcher(version);

				if (!matcher.matches()) {
					errors.add(name + " does not contain 4 parts");
				}
			}
		}
		System.out.println("   Checked " + checked + " of " + totalsize + ".");
		System.out.println("   Errors found: " + errors.size());

		if (errors.size() > 0) {
			Collections.sort(errors);
			for (Iterator iter = errors.iterator(); iter.hasNext();) {
				System.out.println(iter.next());
			}
			System.out.println("Errors in versions. Check error log for details.");
			failuresOccured = true;
		}
		return failuresOccured;
	}

	private String getBasicName(String fullname, String extension) {
		String result = fullname;
		int pos = fullname.lastIndexOf(extension);
		if (pos >= 0) {
			result = fullname.substring(0, pos);
		}
		return result;
	}

	public boolean testFeatureVersions() {

		String property = getDirectoryToCheck();
		assertNotNull("Need to set input directory to check against.", property);
		String featureDirectory = null;
		if (property.endsWith("/")) {
			featureDirectory = property + "features";
		}
		else {
			featureDirectory = property + "/features";
		}
		File inputdir = new File(featureDirectory);
		assertTrue("feature direcotry (" + featureDirectory + ") must be an existing directory.", inputdir.exists() && inputdir.isDirectory());
		System.out.println(" Test Feature Versions");
		File[] children = inputdir.listFiles();
		boolean failuresOccured = checkFilesVersions(inputdir);
		return failuresOccured;

	}
}
