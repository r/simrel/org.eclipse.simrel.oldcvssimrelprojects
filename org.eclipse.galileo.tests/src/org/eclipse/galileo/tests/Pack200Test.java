/*******************************************************************************
 * Copyright (c) 2007, 2009 IBM Corporation and others. All rights reserved. This
 * program and the accompanying materials are made available under the terms of
 * the Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: IBM Corporation - initial API and implementation
 * This file originally came from 'Eclipse Orbit' project then adapted to use 
 * in WTP and improved to use 'Manifest' to read manifest.mf, instead of reading 
 * it as a properties file.
 ******************************************************************************/
package org.eclipse.galileo.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.tools.ant.BuildException;
import org.eclipse.internal.provisional.equinox.p2.jarprocessor.JarProcessor;
import org.eclipse.osgi.util.ManifestElement;
import org.osgi.framework.BundleException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * @since 3.3
 */
public class Pack200Test extends TestCase {

	private static final String EXTENSION_JAR = ".jar";
	private static final String EXTENSION_PACEKD_JAR = ".jar.pack.gz";
	private static final String EXTENSION_ZIP = ".zip";
	private FullJarNameParser nameParser = new FullJarNameParser();
	private String BACKSLASH = "\\";
	private String LITERAL_PERIOD = BACKSLASH + ".";
	private String ANY = ".*";

	public static void main(String[] args) {
		System.out.println("start Pack200Test main");
		Pack200Test testlayout = new Pack200Test();
		testlayout.setDirectoryToCheck("D:\\temptest");
		testlayout.testBundlePack();

	}

	public static Test suite() {
		return new TestSuite(Pack200Test.class);
	}

	private String directoryToCheck;


	public String getDirectoryToCheck() {
		return directoryToCheck;
	}

	public void setDirectoryToCheck(String bundleDirToCheck) {
		this.directoryToCheck = bundleDirToCheck;
	}

	public boolean testBundlePack() {

		String property = getDirectoryToCheck();
		assertNotNull("Need to set input directory to check against.", property);
		String bundleDirectory = null;
		if (property.endsWith("/")) {
			bundleDirectory = property + "plugins";
		}
		else {
			bundleDirectory = property + "/plugins";
		}
		File inputdir = new File(bundleDirectory);
		assertTrue("bundle direcotry (" + bundleDirectory + ") must be an existing directory.", inputdir.exists() && inputdir.isDirectory());
		System.out.println(" List Bundles not packed.");
		boolean failuresOccured = checkFilesPacked(inputdir);
		return failuresOccured;
	}

	private boolean checkFilesPacked(File inputdir) {
		// reset/initialize errors
		List errors = new ArrayList();
		boolean failuresOccured = false;
		File[] children = inputdir.listFiles();
		int totalsize = children.length;
		int checked = 0;
		for (int i = 0; i < totalsize; i++) {
			File child = children[i];
			String name = child.getName();
			// assume directory if not file
			if (child.isFile()) {
				checked++;
				if (!name.endsWith(EXTENSION_PACEKD_JAR)) {
					String basicName = getBasicName(name, EXTENSION_JAR);
					boolean valid = nameParser.parse(basicName);
					String bundlename = basicName;
					if (!valid) {
						errors.add(name + " does not have a valid version (it is unparsable)");
					}
					else {
						if (containsJava(child)) {
							bundlename = nameParser.getProjectString();
							errors.add(name + " is not packed");
						}
					}
				}
			}

		}
		System.out.println("   Checked " + checked + " of " + totalsize + ".");
		System.out.println("   Errors found: " + errors.size());

		if (errors.size() > 0) {
			Collections.sort(errors);
			for (Iterator iter = errors.iterator(); iter.hasNext();) {
				System.out.println(iter.next());
			}
			System.out.println("Errors in pack200 rule. Check error log for details.");
			failuresOccured = true;
		}
		return failuresOccured;
	}

	private boolean containsJava(File jarfile) {
		// We assume the file is a 'jar' file.
		boolean containsJava = false;
		JarFile jar = null;
		try {
			jar = new JarFile(jarfile, false, ZipFile.OPEN_READ);
			Enumeration<JarEntry> entries = jar.entries();
			while (entries.hasMoreElements()) {
				JarEntry entry = entries.nextElement();
				if (entry.getName().endsWith(".class")) {
					containsJava = true;
					break;
				}
				else if (entry.getName().endsWith(".jar")) {
					InputStream input = jar.getInputStream(entry);
					if (containsJava(input)) {
						containsJava = true;
						break;
					}
				}
			}

		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (jar != null)
				try {
					jar.close();
				}
				catch (IOException e) {
					// ignore
				}
		}
		return containsJava;
	}

	private boolean containsJava(InputStream input) {
		// We assume the file is a 'jar' file.
		boolean containsJava = false;

		try {
			JarInputStream jarInputStream = new JarInputStream(input);
			while (jarInputStream.available() > 0) {
				ZipEntry entry = jarInputStream.getNextEntry();
				if (entry != null) {
					if (entry.getName().endsWith(".class")) {
						containsJava = true;
						break;
					}
				}
			}

		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (input != null)
				try {
					input.close();
				}
				catch (IOException e) {
					// ignore
				}
		}
		return containsJava;
	}

	private String getBasicName(String fullname, String extension) {
		String result = fullname;
		int pos = fullname.lastIndexOf(extension);
		if (pos >= 0) {
			result = fullname.substring(0, pos);
		}
		return result;
	}
}
