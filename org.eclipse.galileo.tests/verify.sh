# echo 
# echo $(basename $1
filename="${1}"
jarname=$(basename "${filename}")

export JAVA_HOME=/shared/common/ibm-java2-ppc-50
#JAVA_HOME=/shared/common/ibm-java-ppc-604
#JAVA_HOME=/shared/common/ibm-java-jdk-ppc-60

if [[ -z $VERIFYOUTDIR ]] 
then
    VERIFYOUTDIR="${HOME}"/verifyoutput
fi

if [[ "$jarname" =~ "(.*).pack.gz$" ]]
then 
    basejarname=${BASH_REMATCH[1]}
    #echo -e "\n basejarname: " $basejarname "\n"
    $JAVA_HOME/jre/bin/unpack200 $filename /tmp/$basejarname
    #unpack200 $filename /tmp/$basejarname
    vresult=`$JAVA_HOME/bin/jarsigner -verify /tmp/$basejarname`
    exitcode=$?
    rm /tmp/$basejarname
else
    #echo -e "\n filename: " $filename "\n"
    vresult=`$JAVA_HOME/bin/jarsigner -verify $filename`
    exitcode=$?
fi

if [[ "${vresult}" =~ "^jar verified.*" ]]
then
    printf '%-100s \t\t' "   ${jarname}: " >> "${VERIFYOUTDIR}"/verified.txt 
    printf '%s\n' " ${vresult} " >> "${VERIFYOUTDIR}"/verified.txt
elif [[ "${vresult}" =~ "^jar is unsigned.*" ]]
then

# purposely no line delimiter, so output of jarsigner is on same line
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/unsigned.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/unsigned.txt 
elif [[ "${vresult}" =~ "^no manifest.*" ]]
then

# purposely no line delimiter, so output of jarsigner is on same line
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/nomanifest.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/nomanifest.txt 

else 
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/error.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/error.txt 
fi
 
if [[ $exitcode -gt 0 ]]
then

    echo -e "\n exitcode: " $exitcode: $(basename $filename)" \n"  >> "${VERIFYOUTDIR}"/errorexit.txt  
fi


