#!/usr/bin/env bash

source galileo_properties.shsource

"${ECLIPSE_HOME_TEST}"/eclipse -consolelog -data ./runEclipseTests -nosplash --launcher.suppressErrors -vm "${JAVA_EXEC_DIR}" -application org.eclipse.ant.core.antRunner -f runTests.xml "$@"
