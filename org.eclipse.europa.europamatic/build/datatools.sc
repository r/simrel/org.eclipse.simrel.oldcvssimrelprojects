<?xml version='1.0'?>
<sc:siteContribution xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	rmapProviderURL="${downloads}/datatools/downloads/drops/updates/site.xml">
	<sc:cspec name="org.eclipse.datatools">
		<dependencies>
			<dependency name="org.eclipse.datatools.modelbase.feature" versionDesignator="[1.5.0.200702141]" />
			<dependency name="org.eclipse.datatools.connectivity.feature" versionDesignator="[1.5.0.200702141]" />
			<dependency name="org.eclipse.datatools.connectivity.oda.feature" versionDesignator="[1.5.0.200702141]" />
			<dependency name="org.eclipse.datatools.connectivity.oda.designer.feature" versionDesignator="[1.5.0.200702141]" />
			<dependency name="org.eclipse.datatools.sqldevtools.feature" versionDesignator="[1.5.0.200702141]" />
			<dependency name="org.eclipse.datatools.enablement.feature" versionDesignator="[1.5.0.200702141]" />
			<dependency name="org.eclipse.datatools.enablement.oda.feature" versionDesignator="[1.5.0.200702141]" />
			<dependency name="org.eclipse.datatools.enablement.oda.designer.feature" versionDesignator="[1.5.0.200702141]" />
			<dependency name="org.eclipse.datatools.doc.user" versionDesignator="[1.0.0.200702011]" />
			<dependency name="org.eclipse.datatools.intro" versionDesignator="[1.0.0.200702011]" />
			<dependency name="org.eclipse.datatools.sdk.feature" versionDesignator="[1.5.0.200702141]" />
		</dependencies>
		<groups>
			<public name="Database Development">
				<attribute component="org.eclipse.datatools.modelbase.feature"/>
				<attribute component="org.eclipse.datatools.connectivity.feature"/>
				<attribute component="org.eclipse.datatools.connectivity.oda.feature"/>
				<attribute component="org.eclipse.datatools.connectivity.oda.designer.feature"/>
				<attribute component="org.eclipse.datatools.sqldevtools.feature"/>
				<attribute component="org.eclipse.datatools.enablement.feature"/>
				<attribute component="org.eclipse.datatools.enablement.oda.feature"/>
				<attribute component="org.eclipse.datatools.enablement.oda.designer.feature"/>
				<attribute component="org.eclipse.datatools.doc.user"/>
				<attribute component="org.eclipse.datatools.intro"/>
				<attribute component="org.eclipse.datatools.sdk.feature"/>
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>
