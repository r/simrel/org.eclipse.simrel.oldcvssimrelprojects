# Get all the files for the Europamatic build
# on the build.eclipse.org machine.

# This script should be "copied up" and ran from a
# directory "above" the main working directory of
# org.eclipse.europa.europamatic
# this is typically the users $HOME/europa/ directory

rm -fr org.eclipse.europa.europamatic

cvs -Q -f -d /cvsroot/callisto export -r HEAD org.eclipse.europa.europamatic
mv org.eclipse.europa.europamatic/build/build.eclipse.org.local.properties org.eclipse.europa.europamatic/build/local.properties

chmod -R +x org.eclipse.europa.europamatic/*.sh
chmod -R +x org.eclipse.europa.europamatic/**/*.sh

cp org.eclipse.europa.europamatic/getEuropaToolsFromCVS.sh .

echo "    cd org.eclipse.europa.europamatic/build/"
echo "    screen -dmS europa"
echo "    ./runAll.sh -clean 2>&1 | tee ~/europa/out.txt"
echo " "