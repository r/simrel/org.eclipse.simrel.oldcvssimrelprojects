package org.eclipse.helios.tests;

import java.io.File;
import java.io.FilenameFilter;

class JARFileNameFilter implements FilenameFilter {
    private static final String EXTENSION_JAR = ".jar";

    public boolean accept(File dir, String name) {
        return name.endsWith(EXTENSION_JAR);
    }
}
