package org.eclipse.helios.tests;

import java.io.IOException;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

public class TestLayoutTask extends Task {

    private String  directoryToCheck;
    private String  tempWorkingDir;
    private String  outputResultsDirectory;
    private boolean failuresOccurred = false;

    @Override
    public void execute() throws BuildException {

        try {

            BREETest breeTest = new BREETest();
            breeTest.setDirectoryToCheck(getDirectoryToCheck());
            breeTest.setTempWorkingDir(getTempWorkingDir());
            breeTest.setOutputDirectory(getOutputResultsDirectory());

            boolean breeFailures = breeTest.testBREESettingRule();

            if (breeFailures) {
                setFailuresOccurred(true);
            }


            Pack200Test packTest = new Pack200Test();
            packTest.setDirectoryToCheck(getDirectoryToCheck());
            packTest.setOutputDirectory(getOutputResultsDirectory());
            boolean packFailures = packTest.testBundlePack();
            if (packFailures) {
                setFailuresOccurred(true);
            }


            VersionTest versionTest = new VersionTest();
            versionTest.setDirectoryToCheck(getDirectoryToCheck());
            versionTest.setOutputDirectory(getOutputResultsDirectory());
            boolean versionCheck = versionTest.testVersionsPatterns();
            if (versionCheck) {
                setFailuresOccurred(true);
            }


            TestLayoutTest test = new TestLayoutTest();
            test.setDirectoryToCheck(getDirectoryToCheck());
            test.setTempWorkingDir(getTempWorkingDir());
            test.setOutputDirectory(getOutputResultsDirectory());
            boolean layoutFailures = test.testLayout();
            if (layoutFailures) {
                setFailuresOccurred(true);
            }
        }
        catch (IOException e) {
            throw new BuildException(e);
        }
    }

    public String getDirectoryToCheck() {
        return directoryToCheck;
    }

    public void setDirectoryToCheck(String bundleDirToCheck) {
        this.directoryToCheck = bundleDirToCheck;
    }

    public String getTempWorkingDir() {
        return tempWorkingDir;
    }

    public void setTempWorkingDir(String tempWorkingDir) {
        this.tempWorkingDir = tempWorkingDir;
    }

    public boolean isFailuresOccurred() {
        return failuresOccurred;
    }

    public void setFailuresOccurred(boolean failuresOccurred) {
        this.failuresOccurred = failuresOccurred;
    }

    public String getOutputResultsDirectory() {
        return outputResultsDirectory;
    }

    public void setOutputResultsDirectory(String outputResultsDirectory) {
        this.outputResultsDirectory = outputResultsDirectory;
    }

}
