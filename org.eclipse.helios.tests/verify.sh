# echo 
# echo $(basename $1
filename="${1}"
jarname=$(basename "${filename}")

export JAVA_HOME=/shared/webtools/apps/ibm-java2-sdk-5.0-12.1-linux-i386


if [[ -z $VERIFYOUTDIR ]] 
then
    VERIFYOUTDIR="${HOME}"/verifyoutput
fi
PPAT_PACKGZ="(.*).pack.gz$"
if [[ "$jarname" =~  $PPAT_PACKGZ ]]
then 
    basejarname=${BASH_REMATCH[1]}
    #echo -e "\n basejarname: " $basejarname "\n"
    $JAVA_HOME/jre/bin/unpack200 $filename /tmp/$basejarname
    #unpack200 $filename /tmp/$basejarname
    vresult=`$JAVA_HOME/bin/jarsigner -verify /tmp/$basejarname`
    exitcode=$?
    rm /tmp/$basejarname
else
    #echo -e "\n filename: " $filename "\n"
    vresult=`$JAVA_HOME/bin/jarsigner -verify $filename`
    exitcode=$?
fi

PPAT_VERIFIED="^jar\ verified.*"
PPAT_UNSIGNED="^jar is unsigned.*"
PPAT_NOMANIFEST="^no manifest.*"
if [[ "${vresult}" =~ $PPAT_VERIFIED ]]
then
    printf '%-100s \t\t' "   ${jarname}: " >> "${VERIFYOUTDIR}"/verified.txt 
    printf '%s\n' " ${vresult} " >> "${VERIFYOUTDIR}"/verified.txt
elif [[ "${vresult}" =~ $PPAT_UNSIGNED ]]
then

# purposely no line delimiter, so output of jarsigner is on same line
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/unsigned.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/unsigned.txt 
elif [[ "${vresult}" =~ PPAT_NOMANIFEST ]]
then

# purposely no line delimiter, so output of jarsigner is on same line
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/nomanifest.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/nomanifest.txt 

else 
    printf '%-100s \t\t' "   ${jarname}: "  >> "${VERIFYOUTDIR}"/error.txt 
    printf '%s\n' " ${vresult} "  >> "${VERIFYOUTDIR}"/error.txt 
fi
 
if [[ $exitcode -gt 0 ]]
then

    echo -e "\n exitcode: " $exitcode: $(basename $filename)" \n"  >> "${VERIFYOUTDIR}"/errorexit.txt  
fi


