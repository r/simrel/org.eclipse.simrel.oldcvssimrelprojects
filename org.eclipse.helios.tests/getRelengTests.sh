#!/usr/bin/env bash

# it is assumed we are executing this in RELENG_TESTS or the parent of RELENG_TESTS

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source helios_properties.shsource
  
# This script file is to help get builds started "fresh", when
# the ${RELENG_TESTS} directory already exists on local file system.
# While it is in the cvs repository in ${RELENG_TESTS}, it is
# meant to be executed from the parent directory
# of ${RELENG_TESTS} on the file system.

# export is used, instead of checkout, just to avoid the CVS directories and since this code
# for a local build, there should never be a need to check it back in to CVS.

# If there is no subdirectory, try going up one directory and looking again (in case we are in it).
if [ ! -e ${RELENG_TESTS} ]
then
	cd ..
	if [ ! -e ${RELENG_TESTS} ]
	then	
         echo "${RELENG_TESTS} does not exist as sub directory";
	  exit 1;
	fi
fi


# make sure RELENG_TESTS has been defined and is no zero length, or 
# else following will eval to "rm -fr /*" ... potentially catastrophic
if [ -z "${RELENG_TESTS}" ]
then
   echo "The variable RELENG_TESTS must be defined to run this script"
   exit 1;
fi
echo "    removing all of ${RELENG_TESTS} ..."
rm -fr "${RELENG_TESTS}"/*
rm -fr "${RELENG_TESTS}"/.project
rm -fr "${RELENG_TESTS}"/.settings
rm -fr "${RELENG_TESTS}"/.classpath
mkdir -p "${RELENG_TESTS}"

#controltag=david_williams_tempBranch3
controltag=HEAD
echo "    checking out $controltag of ${RELENG_TESTS} from cvs ..."
cvs -Q -f -d :pserver:anonymous@dev.eclipse.org:/cvsroot/callisto  export -d ${RELENG_TESTS} -r $controltag ${RELENG_TESTS}



echo "    making sure releng control files are executable and have proper EOL ..."
dos2unix ${RELENG_TESTS}/*.sh* ${RELENG_TESTS}/*.properties ${RELENG_TESTS}/*.xml >/dev/null 2>>/dev/null
chmod +x ${RELENG_TESTS}/*.sh > /dev/null
echo

rm -fr ../tests/eclipse/dropins/${RELENG_TESTS}
cp -r ${RELENG_TESTS} tests/eclipse/dropins/
