
if [ -z $1 ]
then
    loc=./
else
    loc=${1}
fi
if [ -z $2 ]
then
    pat=".*\(\.jar$\|\.jar\.pack\.gz$\)"
else
    pat="${2}"
fi

# VERIFYOUTDIR is used in 'verify.sh' and must be clean before
# beginning
export VERIFYOUTDIR="${HOME}"/verifyoutdir

# make, in case doesn't exist
mkdir -p "${VERIFYOUTDIR}"

# even though we just set it above, include the if check 
# here to gaurd against future errors, since if 'verfiyoutdir' is 
# not defined, the remove if for the root directory! 
if [[ -n "${VERIFYOUTDIR}" ]]
then 
       rm -fr "${VERIFYOUTDIR}"/*
fi

echo "   verify directory: ${loc}";
echo "      for pattern:    ${pat}";
echo "   output files in " "${VERIFYOUTDIR}" 

find "${loc}" -regex "${pat}" -exec verify.sh '{}' \;

