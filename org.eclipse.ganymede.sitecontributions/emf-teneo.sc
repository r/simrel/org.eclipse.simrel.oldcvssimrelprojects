<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emf/updates/interim/site-ganymede.xml">
	<sc:member name="EMF TENEO Build Team" email="mtaal@elver.org" />
	<sc:member name="EMF TENEO Build Team" email="stepper@esc-net.de" />
	<sc:cspec name="org.eclipse.emf-teneo-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.teneo.sdk" versionDesignator="[1.0.4.v200906300343-4-7888F4KFMgdUMaT65sYNmutWWG]" />
			<dependency name="org.eclipse.emf.teneo" versionDesignator="[1.0.4.v200906300343-79-77EVVFGMFz0cFFckc]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.emf.teneo.sdk" />
				<attribute component="org.eclipse.emf.teneo" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

