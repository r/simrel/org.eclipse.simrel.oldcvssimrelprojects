<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emf/updates/releases/site-ganymede.xml">
	<sc:member name="EMF NET4J Build Team" email="stepper@esc-net.de" />
	<sc:member name="EMF NET4J Build Team" email="smcduff@hotmail.com" />
	<sc:member name="EMF NET4J Build Team" email="mtaal@elver.org" />
	<sc:member name="EMF NET4J Build Team" email="stefan.winkler-et@fernuni-hagen.de" />
	<sc:member name="EMF NET4J Build Team" email="vroldan@opencanarias.com" />
	<sc:member name="EMF NET4J Build Team" email="dietisheim@puzzle.ch" />
	<sc:cspec name="org.eclipse.emf-net4j-sc">
		<dependencies>
			<dependency name="org.eclipse.net4j.sdk" versionDesignator="[1.0.9.v200902272310]" />
			<dependency name="org.eclipse.net4j" versionDesignator="[1.0.9.v200902272310]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.net4j.sdk" />
				<attribute component="org.eclipse.net4j" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

