<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emf/updates/releases/site-ganymede.xml">
	<sc:member name="EMF Build Team" email="davidms@ca.ibm.com" />
	<sc:cspec name="org.eclipse.mdt-xsd-sc">
		<dependencies>
			<dependency name="org.eclipse.xsd.edit" versionDesignator="[2.4.0.v200902171115]" />
			<dependency name="org.eclipse.xsd.sdk" versionDesignator="[2.4.2.v200902171115]" />
			<dependency name="org.eclipse.xsd" versionDesignator="[2.4.2.v200902171115]" />
		</dependencies>
		<groups>
			<public name="Enabling Features">
				<attribute component="org.eclipse.xsd.edit" />
			</public>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.xsd.sdk" />
				<attribute component="org.eclipse.xsd" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

