<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/m2t/updates/releases/site.xml">
	<sc:member name="M2T JET Build Team" email="pelder@ca.ibm.com" />
	<sc:cspec name="org.eclipse.m2t-jet-sc">
		<dependencies>
			<dependency name="org.eclipse.jet.sdk" versionDesignator="[0.9.2.v200809290937-17Y385Ue8djXGNtNVUY6I48]" />
			<dependency name="org.eclipse.jet" versionDesignator="[0.9.2.v200809290937-27I39oB55V5CBO5H37]" />
			<dependency name="org.eclipse.jet.editor" versionDesignator="[0.9.0.v20080516-17807w311A12372813]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.jet.sdk" />
				<attribute component="org.eclipse.jet" />
				<attribute component="org.eclipse.jet.editor" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

