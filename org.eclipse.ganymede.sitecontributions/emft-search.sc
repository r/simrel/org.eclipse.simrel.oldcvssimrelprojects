<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emft/updates/releases/site-ganymede.xml">
	<sc:member name="EMFT SEARCH Build Team" email="lucas.bigeardel@gmail.com" />
	<sc:member name="EMFT SEARCH Build Team" email="lucas.bigeardel@gmail.com" />
	<sc:cspec name="org.eclipse.emft-search-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.ecoretools.diagram.search" versionDesignator="[0.7.0.v200806130939]" />
			<dependency name="org.eclipse.emf.search.sdk" versionDesignator="[0.7.0.v200806130939]" />
			<dependency name="org.eclipse.emf.search" versionDesignator="[0.7.0.v200806130939]" />
			<dependency name="org.eclipse.uml2.diagram.clazz.search" versionDesignator="[0.7.0.v200806130939]" />
			<dependency name="org.eclipse.uml2.search" versionDesignator="[0.7.0.v200806130939]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.emf.ecoretools.diagram.search" />
				<attribute component="org.eclipse.emf.search.sdk" />
				<attribute component="org.eclipse.emf.search" />
				<attribute component="org.eclipse.uml2.diagram.clazz.search" />
				<attribute component="org.eclipse.uml2.search" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

