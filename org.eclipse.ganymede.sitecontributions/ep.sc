<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/eclipse/updates/3.4.x/site.xml">
    <sc:member
        name="Kim Moir"
        email="kmoir@ca.ibm.com" />
    <sc:cspec name="org.eclipse.ep-sc">
        <dependencies>
            <dependency
                name="org.eclipse.cvs"
                versionDesignator="[1.1.2.R342_v20090122-7C79E9x9sLM1t6M9YD7_A7]" />
            <dependency
                name="org.eclipse.equinox"
                versionDesignator="[3.4.1.R342_v20090126-7w7TENgETuNblxYRhBOLydU3ADDC]" />
            <dependency
                name="org.eclipse.platform"
                versionDesignator="[3.4.2.R342_v20090122-9I96EiWElHi8lheoJKJIvhM3JfVsYbRrgVIWL]" />
            <dependency
                name="org.eclipse.pde"
                versionDesignator="[3.4.2.R342_v20090122-7T7U1E9imVKz-A8Vz-p_jRS]" />
            <dependency
                name="org.eclipse.jdt"
                versionDesignator="[3.4.2.r342_v20081217-7o7tEAoEEDWEm5HTrKn-svO4BbDI]" />
            <dependency
                name="org.eclipse.help"
                versionDesignator="[1.0.2.R342_v20090122-7r7xEKaEJBZu5oGhP3u6nOmbM9VK]" />           
        </dependencies>
        <groups>
            <public name="Collaboration Tools">
                <attribute component="org.eclipse.cvs" />
            </public>
            <public name="Enabling Features">
                <attribute component="org.eclipse.equinox" />
                <attribute component="org.eclipse.platform" />
                <attribute component="org.eclipse.help" />              
            </public>
            <public name="Java Development">
                <attribute component="org.eclipse.jdt" />
                <attribute component="org.eclipse.pde" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
