<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/datatools/downloads/drops/N_updates/site.xml">
    <sc:member
        name="Xiaoying Gu"
        email="xgu@actuate.com" />
    <sc:cspec name="org.eclipse.dtp-sc">
        <dependencies>
            <dependency
                name="org.eclipse.datatools.connectivity.feature"
                versionDesignator="[1.6.2.v200810071455-7_7K7NDn-QYKjkBKlmD4edYsAX]" />
            <dependency
                name="org.eclipse.datatools.connectivity.oda.designer.feature"
                versionDesignator="[1.6.2.v200901211403-7C67XCYQCD7ESIqIQco]" />
            <dependency
                name="org.eclipse.datatools.connectivity.oda.feature"
                versionDesignator="[1.6.2.v200810071455-7F27SBcMAAxCcHmCNWi]" />
            <dependency
                name="org.eclipse.datatools.doc.user"
                versionDesignator="[1.6.2.v200810071455-37A09oA55S6H_L8DFG24]" />
            <dependency
                name="org.eclipse.datatools.common.doc.user"
                versionDesignator="[1.6.2.v200810071455-1507w31191939244824]" />
            <dependency
                name="org.eclipse.datatools.connectivity.doc.user"
                versionDesignator="[1.6.2.v200810071455-27A18s733I3I7I599G59]" />
            <dependency
                name="org.eclipse.datatools.sqltools.doc.user"
                versionDesignator="[1.6.2.v200810071455-1507w31191939244824]" />
            <dependency
                name="org.eclipse.datatools.enablement.apache.derby.feature"
                versionDesignator="[1.6.2.v200810071455-6577AlJCKcLZI2Ljcc8MDD]" />
            <dependency
                name="org.eclipse.datatools.enablement.feature"
                versionDesignator="[1.6.2.v200810071455-7D8H7P9FT9Ofz-RuiDlMiw8OR2pj]" />
            <dependency
                name="org.eclipse.datatools.enablement.hsqldb.feature"
                versionDesignator="[1.6.2.v200810071455-57_2_pFAIQJZIsLSXb8MDD]" />
            <dependency
                name="org.eclipse.datatools.enablement.ibm.feature"
                versionDesignator="[1.6.2.v200810071455-7D47FEB7sQSBV7hXWQ6R]" />
            <dependency
                name="org.eclipse.datatools.enablement.jdbc.feature"
                versionDesignator="[1.6.2.v200810071455-3-29oA55S7__D7LCC]" />
            <dependency
                name="org.eclipse.datatools.enablement.jdt.feature"
                versionDesignator="[1.6.2.v200810071455-1-07w311_12281248]" />
            <dependency
                name="org.eclipse.datatools.enablement.msft.feature"
                versionDesignator="[1.6.2.v200810071455-442_kE77c7VBQDFNW]" />
            <dependency
                name="org.eclipse.datatools.enablement.mysql.feature"
                versionDesignator="[1.6.2.v200810071455-446_kE77d8H_VALOJ]" />
            <dependency
                name="org.eclipse.datatools.enablement.oda.designer.feature"
                versionDesignator="[1.6.2.v200810071455-2138s733J465I548A]" />
            <dependency
                name="org.eclipse.datatools.enablement.oda.feature"
                versionDesignator="[1.6.2.v200810071455-797P78CYQCD5DjGdEURT]" />
            <dependency
                name="org.eclipse.datatools.enablement.oracle.feature"
                versionDesignator="[1.6.2.v200810071455-443_kE77d7OAPAMJQ]" />
            <dependency
                name="org.eclipse.datatools.enablement.postgresql.feature"
                versionDesignator="[1.6.2.v200810071455-442_kE77d7N9VANON]" />
            <dependency
                name="org.eclipse.datatools.enablement.sap.feature"
                versionDesignator="[1.6.2.v200810071455-440_kE77c7QAQEIQQ]" />
            <dependency
                name="org.eclipse.datatools.enablement.sybase.feature"
                versionDesignator="[1.6.2.v200810071455-7A-5E9IgKLjM1d1Oepu]" />
            <dependency
                name="org.eclipse.datatools.intro"
                versionDesignator="[1.6.2.v200810071455-1507w311918132411]" />
            <dependency
                name="org.eclipse.datatools.modelbase.feature"
                versionDesignator="[1.6.2.v200810071455-6-2BcMAAyBWDkEVSb]" />
            <dependency
                name="org.eclipse.datatools.sdk.feature"
                versionDesignator="[1.6.2.v200810071455-67S779KdRRFAuQFpKZ2z-0mp49Vx]" />
            <dependency
                name="org.eclipse.datatools.sqldevtools.data.feature"
                versionDesignator="[1.6.2.v200810071455-3019oA55V599K5M9_]" />
            <dependency
                name="org.eclipse.datatools.sqldevtools.ddl.feature"
                versionDesignator="[1.6.2.v200810071455-25184B23NbbGK_OXQXHz-QP]" />
            <dependency
                name="org.eclipse.datatools.sqldevtools.ddlgen.feature"
                versionDesignator="[1.6.2.v200810071455-77-0CYQCD7FTIWGyPO]" />
            <dependency
                name="org.eclipse.datatools.sqldevtools.feature"
                versionDesignator="[1.6.2.v200810071455-7F7d7AE8yz-YIwEvOFuOF3SqVz-z]" />
            <dependency
                name="org.eclipse.datatools.sqldevtools.parsers.feature"
                versionDesignator="[1.6.2.v200810071455-502AgI99mASCdBYCN]" />
            <dependency
                name="org.eclipse.datatools.sqldevtools.results.feature"
                versionDesignator="[1.6.2.v200810071455-3019oA55U6_7H6KAC]" />
        </dependencies>
        <groups>
            <public name="Enabling Features">
                <attribute component="org.eclipse.datatools.connectivity.feature" />
                <attribute component="org.eclipse.datatools.connectivity.oda.designer.feature" />
                <attribute component="org.eclipse.datatools.connectivity.oda.feature" />
                <attribute component="org.eclipse.datatools.enablement.apache.derby.feature" />
                <attribute component="org.eclipse.datatools.enablement.hsqldb.feature" />
                <attribute component="org.eclipse.datatools.enablement.ibm.feature" />
                <attribute component="org.eclipse.datatools.enablement.jdbc.feature" />
                <attribute component="org.eclipse.datatools.enablement.jdt.feature" />
                <attribute component="org.eclipse.datatools.enablement.msft.feature" />
                <attribute component="org.eclipse.datatools.enablement.mysql.feature" />
                <attribute component="org.eclipse.datatools.enablement.oda.designer.feature" />
                <attribute component="org.eclipse.datatools.enablement.oda.feature" />
                <attribute component="org.eclipse.datatools.enablement.oracle.feature" />
                <attribute component="org.eclipse.datatools.enablement.postgresql.feature" />
                <attribute component="org.eclipse.datatools.enablement.sap.feature" />
                <attribute component="org.eclipse.datatools.enablement.sybase.feature" />
                <attribute component="org.eclipse.datatools.modelbase.feature" />
                <attribute component="org.eclipse.datatools.sqldevtools.data.feature" />
                <attribute component="org.eclipse.datatools.sqldevtools.ddl.feature" />
                <attribute component="org.eclipse.datatools.sqldevtools.ddlgen.feature" />
                <attribute component="org.eclipse.datatools.sqldevtools.parsers.feature" />
                <attribute component="org.eclipse.datatools.sqldevtools.results.feature" />
            </public>
            <public name="Database Development">
                <attribute component="org.eclipse.datatools.doc.user" />
                <attribute component="org.eclipse.datatools.common.doc.user" />
                <attribute component="org.eclipse.datatools.connectivity.doc.user" />
                <attribute component="org.eclipse.datatools.sqltools.doc.user" />
                <attribute component="org.eclipse.datatools.enablement.feature" />
                <attribute component="org.eclipse.datatools.intro" />
                <attribute component="org.eclipse.datatools.sdk.feature" />
                <attribute component="org.eclipse.datatools.sqldevtools.feature" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>


