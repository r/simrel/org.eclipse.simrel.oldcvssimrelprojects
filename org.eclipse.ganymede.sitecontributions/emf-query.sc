<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emf/updates/releases/site-ganymede.xml">
	<sc:member name="EMF QUERY Build Team" email="give.a.damus@gmail.com" />
	<sc:member name="EMF QUERY Build Team" email="nickboldt@gmail.com" />
	<sc:cspec name="org.eclipse.emf-query-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.query.ocl" versionDesignator="[1.2.0.v200805130238-11-7w311916241349]" />
			<dependency name="org.eclipse.emf.query" versionDesignator="[1.2.0.v200805130238-11-7w311916241349]" />
		</dependencies>
		<groups>
			<public name="Enabling Features">
				<attribute component="org.eclipse.emf.query.ocl" />
				<attribute component="org.eclipse.emf.query" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

