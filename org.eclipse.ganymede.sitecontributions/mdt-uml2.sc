<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/mdt/updates/releases/site-ganymede.xml">
	<sc:member name="MDT UML2 Build Team" email="jbruck@ca.ibm.com" />
	<sc:cspec name="org.eclipse.mdt-uml2-sc">
		<dependencies>
			<dependency name="org.eclipse.uml2.sdk" versionDesignator="[2.2.2.v200902101430]" />
			<dependency name="org.eclipse.uml2" versionDesignator="[2.2.2.v200902101430]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.uml2.sdk" />
				<attribute component="org.eclipse.uml2" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

