<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emft/updates/releases/site-ganymede.xml">
	<sc:member name="EMFT MINT Build Team" email="pnehrer@eclipticalsoftware.com" />
	<sc:cspec name="org.eclipse.emft-mint-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.mint.sdk" versionDesignator="[0.7.1.v200809162149]" />
			<dependency name="org.eclipse.emf.mint" versionDesignator="[0.7.1.v200809162149]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.emf.mint.sdk" />
				<attribute component="org.eclipse.emf.mint" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

