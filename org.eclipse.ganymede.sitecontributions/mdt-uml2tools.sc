<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/mdt/updates/releases/site-ganymede.xml">
	<sc:member name="MDT UML2TOOLS Build Team" email="borlander@gmail.com" />
	<sc:member name="MDT UML2TOOLS Build Team" email="michael.golubev@borland.com" />
	<sc:cspec name="org.eclipse.mdt-uml2tools-sc">
		<dependencies>
			<dependency name="org.eclipse.uml2tools.sdk" versionDesignator="[0.8.0.v200809231457]" />
			<dependency name="org.eclipse.uml2tools" versionDesignator="[0.8.1.v200809231457]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.uml2tools.sdk" />
				<attribute component="org.eclipse.uml2tools" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

