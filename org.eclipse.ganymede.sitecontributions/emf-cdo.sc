<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emf/updates/releases/site-ganymede.xml">
	<sc:member name="EMF CDO Build Team" email="stepper@esc-net.de" />
	<sc:member name="EMF CDO Build Team" email="smcduff@hotmail.com" />
	<sc:member name="EMF CDO Build Team" email="mtaal@elver.org" />
	<sc:member name="EMF CDO Build Team" email="stefan.winkler-et@fernuni-hagen.de" />
	<sc:member name="EMF CDO Build Team" email="vroldan@opencanarias.com" />
	<sc:member name="EMF CDO Build Team" email="dietisheim@puzzle.ch" />
	<sc:cspec name="org.eclipse.emf-cdo-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.cdo.sdk" versionDesignator="[1.0.9.v200902280039]" />
			<dependency name="org.eclipse.emf.cdo" versionDesignator="[1.0.9.v200902280039]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.emf.cdo.sdk" />
				<attribute component="org.eclipse.emf.cdo" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

