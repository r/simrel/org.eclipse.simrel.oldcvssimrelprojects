<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emft/updates/releases/site-ganymede.xml">
	<sc:member name="EMFT ECORETOOLS Build Team" email="jacques.lescot@anyware-tech.com" />
	<sc:member name="EMFT ECORETOOLS Build Team" email="jacques.lescot@anyware-tech.com" />
	<sc:cspec name="org.eclipse.emft-ecoretools-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.ecoretools.sdk" versionDesignator="[0.8.0.v200806130600]" />
			<dependency name="org.eclipse.emf.ecoretools" versionDesignator="[0.8.0.v200806130600]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.emf.ecoretools.sdk" />
				<attribute component="org.eclipse.emf.ecoretools" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

