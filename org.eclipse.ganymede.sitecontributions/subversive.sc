<?xml version='1.0' encoding="UTF-8"?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/technology/subversive/0.7/update-site/site.xml">
    <sc:member
        name="Igor Burilo"
        email="igor.burilo@polarion.org"/>
    <sc:cspec name="org.eclipse.subversive-sc">
        <dependencies>
            <dependency
                name="org.eclipse.team.svn"
                versionDesignator="[0.7.8.I20091023-1300]"/>
            <dependency
                name="org.eclipse.team.svn.nl1"
                versionDesignator="[0.7.8.I20091023-1300]"/>
            <dependency
                name="org.eclipse.team.svn.resource.ignore.rules.jdt"
                versionDesignator="[0.7.8.I20091023-1300]"/>
            <dependency
                name="org.eclipse.team.svn.mylyn"
                versionDesignator="[0.7.8.I20091023-1300]"/>
            <dependency
                name="org.eclipse.team.svn.source"
                versionDesignator="[0.7.8.I20091023-1300]"/>
        </dependencies>
        <groups>
            <public name="Collaboration Tools">
                <attribute component="org.eclipse.team.svn"/>
                <attribute component="org.eclipse.team.svn.nl1"/>
                <attribute component="org.eclipse.team.svn.resource.ignore.rules.jdt"/>
                <attribute component="org.eclipse.team.svn.mylyn"/>
                <attribute component="org.eclipse.team.svn.source"/>
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
