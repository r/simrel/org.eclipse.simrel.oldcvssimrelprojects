<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/tools/cdt/updates/ganymede/site.xml">
    <sc:member
        name="Vivian Kong"
        email="vivkong@gmail.com" />
    <sc:member
        name="Doug Schaefer"
        email="dschaefer@rogers.com" />
    <sc:cspec name="org.eclipse.cdt-sc">
        <dependencies>
            <dependency
                name="org.eclipse.cdt.platform"
                versionDesignator="[5.0.2.200902130801]" />
            <dependency
                name="org.eclipse.cdt"
                versionDesignator="[5.0.2.200902130801]" />
            <dependency
            	name="org.eclipse.cdt.mylyn"
            	versionDesignator="[5.0.2.200902130801]" />
        </dependencies>
        <groups>
            <public name="C and C++ Development">
                <attribute component="org.eclipse.cdt.platform" />
                <attribute component="org.eclipse.cdt" />
                <attribute component="org.eclipse.cdt.mylyn" />
            </public>
            <public name="Collaboration Tools">
                <attribute component="org.eclipse.cdt.mylyn" />
            </public>            
        </groups>
    </sc:cspec>
</sc:siteContribution>
