<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/m2m/updates/releases/site-ganymede.xml">
	<sc:member 
		name="Radek Dvorak" 
		email="radek.dvorak@borland.com" />
	<sc:member
        name="Sergey Boyko"
        email="sboyko@borland.com" />
	<sc:cspec name="org.eclipse.m2m-qvtoml-sc">
		<dependencies>
			<dependency name="org.eclipse.m2m.qvt.oml.sdk" versionDesignator="[1.0.1.v20080917-1645-7_-79AAKTAeCFp6h-LgmwTh5l8wU]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.m2m.qvt.oml.sdk" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

