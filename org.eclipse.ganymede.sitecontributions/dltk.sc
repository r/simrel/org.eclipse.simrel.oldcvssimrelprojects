<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/technology/dltk/updates-dev/0.95/site.xml">
    <sc:member
        name="DLTK Build Master"
        email="andrey@xored.com" />
    <sc:cspec name="org.eclipse.dltk-sc">
        <dependencies>
            <dependency
                name="org.eclipse.dltk.core"
                versionDesignator="[0.95.1.v20080626-2043-0EEJ6E9IgKLhLZW8AKz0uq5]" />
            <dependency
                name="org.eclipse.dltk.ruby"
                versionDesignator="[0.95.1.v20080626-2043-0ECq6E8McIJXJUOr9upp-2]" />
            <dependency
                name="org.eclipse.dltk.tcl"
                versionDesignator="[0.95.1.v20080626-2043-0EEJ77E9IgKLhLeUz0ALs1x5]" />
            <dependency
                name="org.eclipse.dltk.xotcl"
                versionDesignator="[0.95.1.v20080903-1434-0Bw2_kE77c7a9N3BFKL]" />
            <dependency
                name="org.eclipse.dltk.itcl"
                versionDesignator="[0.95.1.v20080903-1434-0AR09oA55S5N9J28GBE]" />
            <dependency
                name="org.eclipse.dltk.rse"
                versionDesignator="[0.95.1.v20080903-1434-09y08s733I3H3E15B_7]" />
            <dependency
                name="org.eclipse.dltk.mylyn"
                versionDesignator="[0.95.1.v20080626-2043-08T07w31191_1402545]" />
        </dependencies>
        <groups>
            <public name="Programming Languages">
                <attribute component="org.eclipse.dltk.ruby" />
                <attribute component="org.eclipse.dltk.tcl" />
                <attribute component="org.eclipse.dltk.xotcl" />
                <attribute component="org.eclipse.dltk.itcl" />
            </public>
            <public name="Remote Access and Device Development">
                <attribute component="org.eclipse.dltk.rse" />
            </public>            
            <public name="Collaboration Tools">
                <attribute component="org.eclipse.dltk.mylyn" />
            </public>            
            <public name="Enabling Features">
                <attribute component="org.eclipse.dltk.core" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
