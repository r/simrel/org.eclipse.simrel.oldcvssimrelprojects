<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/rt/ecf/ganymede-update/site.xml">
    <sc:member
        name="Scott Lewis"
        email="slewis@composent.com" />
    <sc:cspec name="org.eclipse.ecf-sc">
        <dependencies>
            <dependency
                name="org.eclipse.ecf.examples"
                versionDesignator="[2.1.0.v20081224-1728]" />
            <dependency
                name="org.eclipse.ecf.core"
                versionDesignator="[2.1.0.v20081224-1728]" />
        </dependencies>
        <groups>
            <public name="Communications">
                <attribute component="org.eclipse.ecf.examples" />
                <attribute component="org.eclipse.ecf.core" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
