<?xml version='1.0' encoding="UTF-8"?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/dsdp/nab/updates/site.xml">
    <sc:member
        name="Shigeki Moride"
        email="moride.shigeki@jp.fujitsu.com" />
    <sc:member
        name="Martin Oberhuber"
        email="martin.oberhuber@windriver.com" />
    <sc:cspec name="org.eclipse.dsdp-nab-sc">
        <dependencies>
            <dependency
                name="org.eclipse.nab.mwt.native.win32"
                versionDesignator="[0.9.9.200806181757]" />
            <dependency
                name="org.eclipse.nab.mwt.native.linux.gtk"
                versionDesignator="[0.9.9.200806181757]" />
        </dependencies>
        <groups>
            <public name="Remote Access and Device Development">
                <attribute component="org.eclipse.nab.mwt.native.win32" />
                <attribute component="org.eclipse.nab.mwt.native.linux.gtk" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
