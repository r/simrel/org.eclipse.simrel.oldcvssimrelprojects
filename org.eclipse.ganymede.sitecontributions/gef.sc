<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/tools/gef/updates/releases/site-ganymede.xml">
	<sc:member name="GEF Build Team" email="anthonyh@ca.ibm.com" />
	<sc:member name="GEF Build Team" email="nickboldt@gmail.com" />
	<sc:cspec name="org.eclipse.gef-sc">
		<dependencies>
			<dependency name="org.eclipse.draw2d.sdk" versionDesignator="[3.4.2.v20090218-1145-67738084A6665K366B84__58x422]" />
			<dependency name="org.eclipse.draw2d" versionDesignator="[3.4.2.v20090218-1145-3317w311_122502441]" />
			<dependency name="org.eclipse.gef.sdk" versionDesignator="[3.4.2.v20090218-1145-7B7E597OKBd7QHgEHNHn9JKTFJJR]" />
			<dependency name="org.eclipse.gef" versionDesignator="[3.4.2.v20090218-1145-67728084A56B4I233613552]" />
			<dependency name="org.eclipse.zest.sdk" versionDesignator="[1.0.0.v20080115-5318_GCGFGJMZHOMaa6PMUULSPXD]" />
			<dependency name="org.eclipse.zest" versionDesignator="[1.0.0.v20080115-5318xB6CE899P233613552]" />
		</dependencies>
		<groups>
			<public name="Enabling Features">
				<attribute component="org.eclipse.draw2d.sdk" />
				<attribute component="org.eclipse.draw2d" />
			</public>
			<public name="Graphical Editors and Frameworks">
				<attribute component="org.eclipse.gef.sdk" />
				<attribute component="org.eclipse.gef" />
				<attribute component="org.eclipse.zest.sdk" />
				<attribute component="org.eclipse.zest" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

