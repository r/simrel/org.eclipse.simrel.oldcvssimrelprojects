<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
   xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
   xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
   updateSite="${downloads}/rt/rap/update-site/site.xml">
   <sc:member
       name="Benny Muskalla"
       email="bmuskalla@innoopract.com" />
   <sc:member
       name="Ruediger Herrmann"
       email="rherrmann@innoopract.com" />
   <sc:cspec name="org.eclipse.rap.tooling-sc">
       <dependencies>
           <dependency
               name="org.eclipse.rap.tooling"
               versionDesignator="[1.1.2.20090218-1359]" />
       </dependencies>
       <groups>
           <public name="Java Development">
               <attribute component="org.eclipse.rap.tooling" />
           </public>
       </groups>
   </sc:cspec>
</sc:siteContribution>
