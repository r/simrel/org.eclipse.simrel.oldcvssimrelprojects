<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/tptp/updates/ganymede/site.xml">
    <sc:member
        name="Joel Cayne"
        email="jcayne@ca.ibm.com" />
    <sc:cspec name="org.eclipse.tptp">
        <dependencies>
        	<dependency
                name="org.eclipse.tptp.lta.runtime"
                versionDesignator="[4.5.1.v200901090956-7V7c08xB1XHKcrfSySMEnN0N52LO]" />        
            <dependency
                name="org.eclipse.tptp.monitoring.runtime"
                versionDesignator="[4.4.101.v200902031919-7V7SEC87zhygHTdxJ6WH9aPaJBP5]" />
            <dependency
                name="org.eclipse.tptp.platform.runtime"
                versionDesignator="[4.5.2.v200901090956-8F82EsM9TofPP4ogyX-bRjDoPYGV]" />
            <dependency
                name="org.eclipse.tptp.test.runtime"
                versionDesignator="[4.3.301.v200902031919-7F7_EFE7SnetoV4PQ4r7amKKnav2]" />
            <dependency
                name="org.eclipse.tptp.trace.runtime"
                versionDesignator="[4.3.201.v200902031919-7F78EAW7VXOsLy-BKPqZLIqZqslF]" />
            <dependency
                name="org.eclipse.tptp.birt"
                versionDesignator="[4.4.201.v200901090956-7J7KAm7i0i7TRQgZX_nhZ_oR034]" />
            <dependency
                name="org.eclipse.tptp.wtp"
                versionDesignator="[4.4.201.v200901090956-784CF7GUGJW0D-yMEcrKLWoHIE8]" />
            <dependency
                name="org.eclipse.tptp.examples"
                versionDesignator="[4.5.1.v200901090956-7B7_Dq87CFtF2_7OF98G_DAA2]" />
        </dependencies>
        <groups>
            <public name="Testing and Performance">
                <attribute component="org.eclipse.tptp.lta.runtime" />
                <attribute component="org.eclipse.tptp.monitoring.runtime" />
                <attribute component="org.eclipse.tptp.platform.runtime" />
                <attribute component="org.eclipse.tptp.test.runtime" />
                <attribute component="org.eclipse.tptp.trace.runtime" />
                <attribute component="org.eclipse.tptp.birt" />                
                <attribute component="org.eclipse.tptp.wtp" />
                <attribute component="org.eclipse.tptp.examples" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
