<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emf/updates/releases/site-ganymede.xml">
	<sc:member name="EMF TRANSACTION Build Team" email="give.a.damus@gmail.com" />
	<sc:member name="EMF TRANSACTION Build Team" email="nickboldt@gmail.com" />
	<sc:cspec name="org.eclipse.emf-transaction-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.transaction" versionDesignator="[1.2.1.v200807161719-2308s733I3E5B4B7J]" />
			<dependency name="org.eclipse.emf.workspace" versionDesignator="[1.2.3.v200810041753-2328s733J3_56398B]" />
			<dependency name="org.eclipse.emf.emfqtv.all.sdk" versionDesignator="[1.2.3.v200810041753-3539-FFBRDMVOp5jnPbgRbDUVz0G]" />
			<dependency name="org.eclipse.emf.emfqtv.all" versionDesignator="[1.2.3.v200810041753-67A3_sOCSMOaKz-GhWyDbXcddQEE]" />
		</dependencies>
		<groups>
			<public name="Enabling Features">
				<attribute component="org.eclipse.emf.transaction" />
				<attribute component="org.eclipse.emf.workspace" />
			</public>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.emf.emfqtv.all.sdk" />
				<attribute component="org.eclipse.emf.emfqtv.all" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

