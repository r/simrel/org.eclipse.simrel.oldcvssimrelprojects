<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emf/updates/releases/site-ganymede.xml">
	<sc:member name="EMF VALIDATION Build Team" email="give.a.damus@gmail.com" />
	<sc:member name="EMF VALIDATION Build Team" email="nickboldt@gmail.com" />
	<sc:cspec name="org.eclipse.emf-validation-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.validation.ocl" versionDesignator="[1.2.0.v200805130238-11-7w311916241349]" />
			<dependency name="org.eclipse.emf.validation" versionDesignator="[1.2.0.v200805130238-35-9oA55S5L8G6FCT]" />
		</dependencies>
		<groups>
			<public name="Enabling Features">
				<attribute component="org.eclipse.emf.validation.ocl" />
				<attribute component="org.eclipse.emf.validation" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

