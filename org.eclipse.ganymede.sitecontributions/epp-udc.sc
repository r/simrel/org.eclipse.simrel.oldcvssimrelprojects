<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
  xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
  xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
  updateSite="${downloads}/technology/epp/updates/1.0milestones/site.xml">
  <sc:member 
    name="Wayne Beaton" 
    email="wayne@eclipse.org" />
  <sc:member
    name="Markus Knauer"
    email="mknauer@innoopract.com" />
	<sc:cspec name="org.eclipse.epp-udc-sc">
		<dependencies>
			<dependency 
				name="org.eclipse.epp.usagedata.feature" 
				versionDesignator="[1.0.1.R200809220400]" /> 
		</dependencies>
		<groups>
			<public name="Other Tools">
				<attribute component="org.eclipse.epp.usagedata.feature" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

