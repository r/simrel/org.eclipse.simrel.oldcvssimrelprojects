<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/mdt/updates/releases/site-ganymede.xml">
	<sc:member name="MDT OCL Build Team" email="give.a.damus@gmail.com" />
	<sc:member name="MDT OCL Build Team" email="nickboldt@gmail.com" />
	<sc:cspec name="org.eclipse.mdt-ocl-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.ocl" versionDesignator="[1.1.101.v200808291106-108Z7w3119193_2217]" />
			<dependency name="org.eclipse.ocl.all" versionDesignator="[1.2.3.v200810101725-348f7__HvvCDBjJNMXObIP8]" />
			<dependency name="org.eclipse.ocl.uml" versionDesignator="[1.2.3.v200811021759-1127w311_12182518]" />
			<dependency name="org.eclipse.ocl" versionDesignator="[1.2.3.v200810101725-3439oA55T5F8H8PAB]" />
			<dependency name="org.eclipse.ocl.all.sdk" versionDesignator="[1.2.3.v200810101725-67_8k_-NRWdNZaZIsiv0nu2bJqYd]" />
		</dependencies>
		<groups>
			<public name="Enabling Features">
				<attribute component="org.eclipse.emf.ocl" />
				<attribute component="org.eclipse.ocl.uml" />
				<attribute component="org.eclipse.ocl" />
			</public>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.ocl.all" />
				<attribute component="org.eclipse.ocl.all.sdk" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

