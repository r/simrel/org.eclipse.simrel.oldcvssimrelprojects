<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emf/updates/releases/site-ganymede.xml">
	<sc:member name="EMF Build Team" email="davidms@ca.ibm.com" />
	<sc:cspec name="org.eclipse.emf-emf-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.common.ui" versionDesignator="[2.4.0.v200902171115]" />
			<dependency name="org.eclipse.emf.common" versionDesignator="[2.4.0.v200902171115]" />
			<dependency name="org.eclipse.emf.ecore.edit" versionDesignator="[2.4.1.v200902171115]" />
			<dependency name="org.eclipse.emf.ecore.sdo.sdk" versionDesignator="[2.4.0.v200902171115]" />
			<dependency name="org.eclipse.emf.ecore.sdo" versionDesignator="[2.4.0.v200902171115]" />
			<dependency name="org.eclipse.emf.ecore" versionDesignator="[2.4.2.v200902171115]" />
			<dependency name="org.eclipse.emf.edit.ui" versionDesignator="[2.4.2.v200902171115]" />
			<dependency name="org.eclipse.emf.edit" versionDesignator="[2.4.2.v200902171115]" />
			<dependency name="org.eclipse.emf.sdk" versionDesignator="[2.4.2.v200902171115]" />
			<dependency name="org.eclipse.emf" versionDesignator="[2.4.2.v200902171115]" />
		</dependencies>
		<groups>
			<public name="Enabling Features">
				<attribute component="org.eclipse.emf.common.ui" />
				<attribute component="org.eclipse.emf.common" />
				<attribute component="org.eclipse.emf.ecore.edit" />
				<attribute component="org.eclipse.emf.ecore" />
				<attribute component="org.eclipse.emf.edit.ui" />
				<attribute component="org.eclipse.emf.edit" />
			</public>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.emf.ecore.sdo.sdk" />
				<attribute component="org.eclipse.emf.ecore.sdo" />
				<attribute component="org.eclipse.emf.sdk" />
				<attribute component="org.eclipse.emf" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

