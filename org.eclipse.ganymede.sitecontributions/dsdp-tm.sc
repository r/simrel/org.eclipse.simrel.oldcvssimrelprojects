<?xml version='1.0' encoding="UTF-8"?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/dsdp/tm/updates/3.0milestones/site-europa.xml">
    <sc:member
        name="Martin Oberhuber"
        email="martin.oberhuber@windriver.com" />
    <sc:member
        name="David Dykstal"
        email="dykstal@acm.org" />
    <sc:cspec name="org.eclipse.dsdp-tm-sc">
        <dependencies>
            <dependency
                name="org.eclipse.rse"
                versionDesignator="[3.0.3.v200902042310-7H37E8qfisI6diJKMbrhR-TV-a3]" />
            <dependency
                name="org.eclipse.rse.remotecdt"
                versionDesignator="[2.1.1.v200809041200-4118s733I3J4F5C48]" />
            <dependency
                name="org.eclipse.rse.useractions"
                versionDesignator="[1.1.2.v200812041720-2138s733I573A5G73]" />
            <dependency
                name="org.eclipse.tm.terminal.sdk"
                versionDesignator="[2.0.3.v200902181600-7J-7EB-NLwTYlTNN4PlbFLKRNHYG]" />
            <dependency
                name="org.eclipse.tm.discovery"
                versionDesignator="[3.0.0.v20080530-7N-E8ME8McIJXIwdh]" />
        </dependencies>
        <groups>
            <public name="Remote Access and Device Development">
                <attribute component="org.eclipse.rse" />
                <attribute component="org.eclipse.rse.remotecdt" />
                <attribute component="org.eclipse.rse.useractions" />
                <attribute component="org.eclipse.tm.terminal.sdk" />
                <attribute component="org.eclipse.tm.discovery" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
