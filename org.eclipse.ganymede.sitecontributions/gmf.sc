<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/gmf/updates/releases/site-ganymede.xml">
	<sc:member name="GMF Build Team" email="gmf-releng@eclipse.org" />
	<sc:cspec name="org.eclipse.gmf-sc">
		<dependencies>
			<dependency name="org.eclipse.gmf.sdk" versionDesignator="[2.1.3.v20090122-1525-7A7879AIkpv6FZKxKlVYDv7PguLk]" />
			<dependency name="org.eclipse.gmf" versionDesignator="[1.1.3.v20090122-1525-7c7a7gEWvhKvnd6yDGljor8ukFgQ]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.gmf.sdk" />
				<attribute component="org.eclipse.gmf" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

