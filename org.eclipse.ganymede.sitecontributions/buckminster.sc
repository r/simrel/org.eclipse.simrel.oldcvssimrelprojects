<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/tools/buckminster/updates-ganymede/site.xml">
    <sc:member
        name="Thomas Hallgren"
        email="thomas@tada.se" />
    <sc:cspec name="org.eclipse.buckminster-sc">
        <dependencies>
            <dependency
                name="org.eclipse.buckminster.core.feature"
                versionDesignator="[1.1.0.r09948]" />
            <dependency
                name="org.eclipse.buckminster.cvs.feature"
                versionDesignator="[1.0.0.r09930]" />
            <dependency
                name="org.eclipse.buckminster.maven.feature"
                versionDesignator="[1.1.0.r09904]" />
            <dependency
                name="org.eclipse.buckminster.p4.feature"
                versionDesignator="[1.0.0.r09908]" />
            <dependency
                name="org.eclipse.buckminster.pde.feature"
                versionDesignator="[1.1.340.r09942]" />
            <dependency
                name="org.eclipse.buckminster.remote.feature"
                versionDesignator="[1.0.0.r09842]" />
        </dependencies>
        <groups>
            <public name="Other Tools">
                <attribute component="org.eclipse.buckminster.core.feature" />
                <attribute component="org.eclipse.buckminster.cvs.feature" />
                <attribute component="org.eclipse.buckminster.maven.feature" />
                <attribute component="org.eclipse.buckminster.p4.feature" />
                <attribute component="org.eclipse.buckminster.pde.feature" />
                <attribute component="org.eclipse.buckminster.remote.feature" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
