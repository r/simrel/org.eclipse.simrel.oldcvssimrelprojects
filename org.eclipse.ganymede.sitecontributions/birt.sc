<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/birt/update-site/2.3-interim/site.xml">
    <sc:member
        name="Xiaoying Gu"
        email="xgu@actuate.com" />
    <sc:member
        name="Wenfeng Li"
        email="wli@actuate.com" />
    <sc:member
        name="Yong Jiang"
        email="yjiang@actuate.com" />
    <sc:cspec name="org.eclipse.birt-sc">
        <dependencies>
            <dependency
                name="com.lowagie.itext"
                versionDesignator="[1.5.4.v20080225-1437w31191339]" />
            <dependency
                name="org.apache.commons.codec"
                versionDesignator="[1.3.0.v20070920-12-7w3119164102711]" />
            <dependency
                name="org.mozilla.rhino"
                versionDesignator="[1.6.7.v20071217-1567w31191325]" />
            <dependency
                name="org.w3c.sac"
                versionDesignator="[1.3.0.v20080102-46-_kE77b7U_N56BA]" />
            <dependency
                name="org.apache.derby.core"
                versionDesignator="[10.3.1.4]" />
            <dependency
                name="org.eclipse.birt"
                versionDesignator="[2.3.2.r232_v20081010-7N7Y7MAIe2QJZSgfOaAphGBR4t7b]" />
            <dependency
                name="org.eclipse.birt.doc"
                versionDesignator="[2.3.2.r232_v20081010-2217s343Aw31192216]" />
            <dependency
                name="org.eclipse.birt.integration.wtp"
                versionDesignator="[2.3.2.r232_v20081204-2217s343A311_1233]" />
            <dependency
                name="org.eclipse.birt.chart.integration.wtp"
                versionDesignator="[2.3.2.r232_v20081010-2217s343A3119211_]" />
        </dependencies>
        <groups>
            <public name="Charting and Reporting">
                <attribute component="org.eclipse.birt" />
                <attribute component="org.eclipse.birt.doc" />
                <attribute component="org.eclipse.birt.integration.wtp" />
                <attribute component="org.eclipse.birt.chart.integration.wtp" />
            </public>
            <public name="Enabling Features">
                <attribute component="com.lowagie.itext" />
                <attribute component="org.apache.commons.codec" />
                <attribute component="org.mozilla.rhino" />
                <attribute component="org.w3c.sac" />
                <attribute component="org.apache.derby.core" />
            </public>
        </groups>
    </sc:cspec>
    
</sc:siteContribution>
