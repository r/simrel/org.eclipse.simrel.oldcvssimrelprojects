<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/tools/mylyn/update/ganymede/site.xml">
    <sc:member
        name="Mik Kersten"
        email="mik.kersten@tasktop.com" />
    <sc:cspec name="org.eclipse.mylyn-sc">
        <dependencies>
            <dependency
                name="org.eclipse.mylyn_feature"
                versionDesignator="[3.0.5.v20090218-1800-e3x]" />
            <dependency
                name="org.eclipse.mylyn.bugzilla_feature"
                versionDesignator="[3.0.5.v20090218-1800-e3x]" />
            <dependency
                name="org.eclipse.mylyn.pde_feature"
                versionDesignator="[3.0.5.v20090218-1800-e3x]" />
            <dependency
                name="org.eclipse.mylyn.java_feature"
                versionDesignator="[3.0.5.v20090218-1800-e3x]" />
            <dependency
                name="org.eclipse.mylyn.ide_feature"
                versionDesignator="[3.0.5.v20090218-1800-e3x]" />
            <dependency
                name="org.eclipse.mylyn.context_feature"
                versionDesignator="[3.0.5.v20090218-1800-e3x]" />
			<dependency
                name="org.eclipse.mylyn.team_feature"
                versionDesignator="[3.0.5.v20090218-1800-e3x]" />
        </dependencies>
        <groups>
            <public name="Collaboration Tools">
                <attribute component="org.eclipse.mylyn_feature" />
                <attribute component="org.eclipse.mylyn.bugzilla_feature" />
                <attribute component="org.eclipse.mylyn.pde_feature" />
                <attribute component="org.eclipse.mylyn.java_feature" />
                <attribute component="org.eclipse.mylyn.ide_feature" />
                <attribute component="org.eclipse.mylyn.context_feature" />
                <attribute component="org.eclipse.mylyn.team_feature" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
