<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/emft/updates/milestones/site-ganymede.xml">
	<sc:member name="EMFT COMPARE Build Team" email="cedric.brun@obeo.fr" />
	<sc:cspec name="org.eclipse.emft-compare-sc">
		<dependencies>
			<dependency name="org.eclipse.emf.compare.sdk" versionDesignator="[0.8.1.v200902100450]" />
			<dependency name="org.eclipse.emf.compare" versionDesignator="[0.8.1.v200902100450]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.emf.compare.sdk" />
				<attribute component="org.eclipse.emf.compare" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

