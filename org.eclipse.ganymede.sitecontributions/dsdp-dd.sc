<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/dsdp/dd/updates/site.xml">
    <sc:member
        name="Ted Williams"
        email="ted.williams@windriver.com" />
    <sc:cspec name="org.eclipse.dsdp-dd-sc">
        <dependencies>
            <dependency
                name="org.eclipse.dd.dsf.sdk"
                versionDesignator="[1.0.0.v20080609]" />
            <dependency
                name="org.eclipse.dd.dsf.gdb_launch"
                versionDesignator="[1.0.0.v20080609]" />
            <dependency
                name="org.eclipse.dd.dsf.gdb"
                versionDesignator="[1.0.0.v20080609]" />
            <dependency
                name="org.eclipse.dd.debug.memory.renderings"
                versionDesignator="[1.0.0.v20080609]" />
            <dependency
                name="org.eclipse.dd.ipxact"
                versionDesignator="[0.9.0.v20080609]" />
        </dependencies>
        <groups>
            <public name="Remote Access and Device Development">
                <attribute component="org.eclipse.dd.dsf.sdk" />
                <attribute component="org.eclipse.dd.dsf.gdb" />
                <attribute component="org.eclipse.dd.dsf.gdb_launch" />
                <attribute component="org.eclipse.dd.debug.memory.renderings" />
                <attribute component="org.eclipse.dd.ipxact" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>

