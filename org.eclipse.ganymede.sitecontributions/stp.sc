<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
    xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
    xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
    updateSite="${downloads}/stp/updates/ganymede/site.xml">
    <sc:member
        name="Oisin Hurley"
        email="ohurley@iona.com" />
    <sc:cspec name="org.eclipse.stp-sc">
        <dependencies>
          <dependency name="org.eclipse.stp.b2j.feature" versionDesignator="[1.0.0.200806121858-2--7733I3E575ICI]" />
          <dependency name="org.eclipse.stp.bpmn.feature" versionDesignator="[1.0.0.200806121858-4--7E77c7UAEAcQc]" />
          <dependency name="org.eclipse.stp.eid.feature" versionDesignator="[0.8.0.200806121858-17k-8IECyKmLOPyly6]" />
          <dependency name="org.eclipse.stp.im.feature" versionDesignator="[1.0.0.200806121858-7_--7cIJXJDScTY5X]" />
          <dependency name="org.eclipse.stp.policy.feature" versionDesignator="[1.0.0.200806121858-78--8MGF6MuOST6t66]" />
          <dependency name="org.eclipse.stp.sca.feature" versionDesignator="[1.0.0.200806121858-7_--7cIJXJDScTY5X]" />
          <dependency name="org.eclipse.stp.sc.jaxws.feature" versionDesignator="[0.9.0.200806121858-67O-_-GAYYSMGWaja7]" />
          <dependency name="org.eclipse.stp.sc.sca.feature" versionDesignator="[0.9.0.200806121858-07P-7A55S5M8A8SJS]" />
          <dependency name="org.eclipse.stp.servicecreation" versionDesignator="[0.9.0.200806121858-07h-7I99m9cDIDmXm]" />
          <dependency name="org.eclipse.stp.soas.feature" versionDesignator="[0.8.0.200806121858-07k-7MAAwAkGMGwew]" />
          <dependency name="org.eclipse.stp.soas.runtime.feature" versionDesignator="[0.8.0.200806121858-07M-7A55S5M8A8SJS]" />
        </dependencies>
        <groups>
            <public name="SOA Development">
                  <attribute component="org.eclipse.stp.b2j.feature" />
                  <attribute component="org.eclipse.stp.bpmn.feature" />
                  <attribute component="org.eclipse.stp.eid.feature" />
                  <attribute component="org.eclipse.stp.im.feature" />
                  <attribute component="org.eclipse.stp.policy.feature" />
                  <attribute component="org.eclipse.stp.sca.feature" />
                  <attribute component="org.eclipse.stp.sc.jaxws.feature" />
                  <attribute component="org.eclipse.stp.sc.sca.feature" />
                  <attribute component="org.eclipse.stp.servicecreation" />
                  <attribute component="org.eclipse.stp.soas.feature" />
                  <attribute component="org.eclipse.stp.soas.runtime.feature" />
            </public>
        </groups>
    </sc:cspec>
</sc:siteContribution>
