<?xml version="1.0" encoding="UTF-8" ?>
<sc:siteContribution
	xmlns="http://www.eclipse.org/buckminster/CSpec-1.0"
	xmlns:sc="http://www.eclipse.org/buckminster/SiteContribution-1.0"
	updateSite="${downloads}/modeling/m2m/updates/releases/site-ganymede.xml">
	<sc:member name="M2M ATL Build Team" email="william.piers@obeo.fr" />
	<sc:cspec name="org.eclipse.m2m-atl-sc">
		<dependencies>
			<dependency name="org.eclipse.m2m.atl.sdk" versionDesignator="[2.0.2.v200812191010]" />
			<dependency name="org.eclipse.m2m.atl" versionDesignator="[2.0.2.v200812191010]" />
		</dependencies>
		<groups>
			<public name="Models and Model Development">
				<attribute component="org.eclipse.m2m.atl.sdk" />
				<attribute component="org.eclipse.m2m.atl" />
			</public>
		</groups>
	</sc:cspec>
</sc:siteContribution>

