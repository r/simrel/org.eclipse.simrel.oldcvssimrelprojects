#!/bin/sh

# script to copy Callisto update jars from their staging to releases area

toDir=~/downloads/callisto/releases
fromDir=~/downloads/callisto/staging

cp -f -p -r -v --update $fromDir/*html --target-directory=$toDir/
cp -f -p -r -v --update $fromDir/*.gif  --target-directory=$toDir/
cp -f -p -r -v --update $fromDir/web  --target-directory=$toDir/
cp -f -p -r -v --update $fromDir/*.xml --target-directory=$toDir/

perl -pi -e 's/callisto\/staging/callisto\/releases/g' ${toDir}/index.html
perl -pi -e 's/callisto\/staging/callisto\/releases/g' ${toDir}/site.xml
perl -pi -e 's/callisto\/staging/callisto\/releases/g' ${toDir}/site-byProject.xml

# important to make sure digest is updated same time as site.xml's
cp -f -p -r -v --update $fromDir/digest.zip --target-directory=$toDir/




