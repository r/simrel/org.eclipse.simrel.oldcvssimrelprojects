
if [ "$1" != "-doit" ] ; then
   echo "    This is a dry run. Add -doit to actually remove"
   doit="--dry-run"
else
   doit=""
fi

rsync $doit --delete --ignore-existing -rv ~/downloads/callisto/staging/plugins ~/downloads/callisto/releases/

rsync $doit --delete --ignore-existing -rv ~/downloads/callisto/staging/features ~/downloads/callisto/releases/

if [ "$doit" = "--dry-run" ] ; then
   echo "    This was a dry run. Add -doit to actually remove"
fi
