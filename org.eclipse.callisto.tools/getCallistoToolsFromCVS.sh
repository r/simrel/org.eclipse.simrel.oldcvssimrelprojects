
# This script should be "copied up" and ran from a
# directory "above" the main working directories of
# org.eclipse.callisto.updatesite, and
# org.eclipse.callisto.tools
# this is typically the users $HOME directory

echo "   "
echo "   Removing old  org.eclipse.callisto.* and exporting fresh HEAD versions." 
echo "   "

echo "        Removing org.eclipse.callisto.updatesite."
rm -fr org.eclipse.callisto.updatesite
echo "        Removing org.eclipse.callisto.tools." 
echo "   "
rm -fr org.eclipse.callisto.tools

# ISSUE: we should move away from using 'head' version
# Note: we can use "local host" since we are running on the same cluser as the cvs server
# otherwise, if running remotely, localhos should be changed to 'dev.eclpise.org'
cvs -Q -f -d /cvsroot/callisto export -r HEAD org.eclipse.callisto.updatesite
cvs -Q -f -d /cvsroot/callisto export -r HEAD org.eclipse.callisto.tools

chmod -R +x org.eclipse.callisto.tools/*.sh
chmod -R +x org.eclipse.callisto.tools/**/*.sh

echo "    for complete refresh, use something similar to following"
echo " " 
echo "    cd org.eclipse.callisto.tools/build-home/"
echo "    screen -x"
echo "    ./runUpdateAllProjects.sh -clean 2>&1 | tee ~/callisto/out.txt"
echo "  "
echo "    When complete, be sure to inspect or at least grep the out.txt file for anomilies, e.g. "
echo "        grep -i failed       out.txt"
echo "        grep -i error        out.txt"
echo "        grep -i exception    out.txt"
echo "        grep -i "could not"  out.txt"
echo " "