/*******************************************************************************
 * Copyright (c) 2004 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     
 *******************************************************************************/

package org.eclipse.callisto.tools.utils;


public class ProjectInfoPair {
	private String projectId;
	private LineInfo lineInfo;
	public LineInfo getLineInfo() {
		return lineInfo;
	}
	public String getProjectId() {
		return projectId;
	}
	public ProjectInfoPair(String projectId, LineInfo lineInfo) {
		this.projectId = projectId;
		this.lineInfo = lineInfo;
	}

}
