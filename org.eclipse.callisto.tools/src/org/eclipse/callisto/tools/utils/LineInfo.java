/*******************************************************************************
 * Copyright (c) 2004 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     
 *******************************************************************************/

package org.eclipse.callisto.tools.utils;


public class LineInfo {

	private String lineInfo;
	private Version version;
	
	public LineInfo(Version version, String line) {
		this.version = version;
		this.lineInfo = line;
	}
	
	public Version getVersion() {
		return version;
	}
	public String getLine() {
		return lineInfo;
	}
}
