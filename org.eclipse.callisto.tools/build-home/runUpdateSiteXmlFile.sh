#!/bin/sh

# script to update site xml files, by merging new features from "mirror" command, with old "authored" content.

main_name=$1

in_authored=~/callisto/org.eclipse.callisto.updatesite/WebContent/$main_name
# remember, this 'site.xml' is just the output of mirror command, when ran in context
in_newFeatures=~/downloads/callisto/staging/.site.xml

# merges newFeautes in to authored, back in original locaion
java -jar siteFileUpdater.jar $in_authored $in_newFeatures

#copy to discover site
cp $in_authored  ~/downloads/callisto/staging/$main_name


