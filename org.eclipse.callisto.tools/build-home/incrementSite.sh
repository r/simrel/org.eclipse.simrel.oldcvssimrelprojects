#!/bin/sh

# script to run incremental update. Only new things in staging, 
# will be copied to releases, so less work for those mirroring
# releases with rsync

stagingSite=file://$HOME/downloads/callisto/staging/site.xml

releasesSite=$HOME/downloads/callisto/testUpdates
 
eclipseLocation=$HOME/eclipse-ppc-33M1

ant  -f incrementSite.xml -DeclipseLocation=$eclipseLocation -DstagingSite=$stagingSite -DreleasesSite=$releasesSite -Dosgi.arch=ppc -Dosgi.clean -Dosgi.debug=true -Declipse.consoleLog=true -DcontinueOnError=true


