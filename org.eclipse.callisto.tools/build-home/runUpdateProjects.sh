#!/bin/sh

# script to run update, setting properties, for an individual project

 updateSite=$HOME/downloads/callisto/staging
 
 eclipseLocation=$HOME/eclipse-ppc-33M1


project="$1"
if [ "$project" !=  "" ] 
then 
  	
     ant  -f features-$project.xml -DeclipseLocation=$eclipseLocation -DlocalUpdateSitePath=$updateSite -Dosgi.arch=ppc -Dosgi.clean -Dosgi.debug=true -Declipse.consoleLog=true -DcontinueOnError=true


else 
     echo "   Usage: $0 <project> (where project is ep, wtp, birt, emf, cdt, dtp, gef, gmf, tptp, ve, batik)"

fi
