#!/bin/sh

# script to run update, setting properties, for all projects


ulimit -n 2014 
export JAVA_HIGH_ZIPFDS=500 

updateSite=$HOME/downloads/callisto/staging
 
eclipseLocation=$HOME/callisto/eclipse-33M4



if [ "$1" == "-clean" ] ; then

	 echo "   Removing $updateSite/features/"
	 rm -fr $updateSite/features/
	
	 echo "   Removing $updateSite/plugins/"
	 rm -fr $updateSite/plugins/
	
	 echo "   Removing index and site xml files"
	 rm -fr $updateSite/*
	
	 echo "   Updating index and web site files, but not site xml files (yet)"
	    cp $HOME/callisto/org.eclipse.callisto.updatesite/WebContent/index.html  $updateSite
	    cp $HOME/callisto/org.eclipse.callisto.updatesite/WebContent/*.jpg  $updateSite > /dev/null
	    cp $HOME/callisto/org.eclipse.callisto.updatesite/WebContent/*.gif  $updateSite > /dev/null
	    cp -r $HOME/callisto/org.eclipse.callisto.updatesite/WebContent/web $updateSite > /dev/null

fi


ant  -f updateMirrorAll.xml -DeclipseLocation=$eclipseLocation -DlocalUpdateSitePath=$updateSite

# merge authored site.xml files with the most recent auto generated one
$HOME/callisto/org.eclipse.callisto.tools/build-home/runUpdateSiteXmlFiles.sh


ant  -f createPack200s.xml     -Declipse.home=$eclipseLocation -DupdateSite=$updateSite

# be sure digest created after the zipped jars
ant  -f createDigests.xml      -Declipse.home=$eclipseLocation -DupdateSite=$updateSite





