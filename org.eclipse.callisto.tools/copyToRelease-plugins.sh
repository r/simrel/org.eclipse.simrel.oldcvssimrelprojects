#!/bin/sh

# script to copy Callisto update jars from their staging to releases area

toDir=~/downloads/callisto/releases
fromDir=~/downloads/callisto/staging

cp -f -p -r -v --update $fromDir/features --target-directory=$toDir/
cp -f -p -r -v --update $fromDir/plugins  --target-directory=$toDir/

