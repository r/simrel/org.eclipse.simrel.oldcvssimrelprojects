
if [ "$1" != "-doit" ] ; then
   echo "    This is a dry run. Add -doit to actually remove"
   doit="--dry-run" 
else 
   doit=""
fi 

 stagingSite=$HOME/downloads/callisto/staging/
releasesSite=$HOME/downloads/callisto/releases/


rsync $doit --ignore-existing -rv $stagingSite/plugins $releasesSite

rsync $doit --ignore-existing -rv $stagingSite/features $releasesSite

if [ "$doit" == "--dry-run" ] ; then
   echo ""
   echo "    This was a dry run. Add -doit to actually remove"
   echo ""
fi
