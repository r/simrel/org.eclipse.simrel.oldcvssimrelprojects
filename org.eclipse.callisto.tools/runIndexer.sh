#!/bin/sh

# OSGi Indexer. See
# http://bundles.osgi.org/bindex.php

# note: the bindex.jar must be obtained from OSGi member, it
# is not part of Eclipse

# must be ran literally from root of repository ... else file paths all off.
# todo: fix this issue with paths, so can be ran from ~/

java -jar ~/osgiIndexer/bindex.jar -name Eclipse -l http://www.eclipse.org/org/documents/epl-v10.php  -t http://download.eclipse.org/callisto/releases/plugins/%f -q plugins/*.jar -r repository.zip

