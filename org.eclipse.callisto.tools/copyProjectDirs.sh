#!/bin/sh

# script to copy Callisto update jars from their "drop" area  

# obsolete - not to be used, will be deleted soon

toDir=/home/data/users/david_williams/downloads/callisto/testUpdates



fromDir=/home/data/httpd/download.eclipse.org/eclipse/updates/3.2milestones

cp -r -v --update $fromDir/features --target-directory=$toDir
cp -r -v --update $fromDir/plugins  --target-directory=$toDir

fromDir=/home/data/httpd/download.eclipse.org/tools/emf/updates

cp -r -v --update $fromDir/features/*_2.2.0.* --target-directory=$toDir/features/
cp -r -v --update $fromDir/plugins/*_2.2.0.*  --target-directory=$toDir/plugins/
cp -r -v --update $fromDirplugins/org.eclipse.emf.ecore.change.edit_2.1.0v20060224* --target-directory=$toDir/plugins/
cp -r -v --update $fromDirplugins/org.eclipse.emf.codegen.ui_2.1.0v20060224*  --target-directory=$toDir/plugins/


fromDir=/home/data/httpd/download.eclipse.org/tools/gef/milestones/update-site/

cp -r -v --update $fromDir/features --target-directory=$toDir
cp -r -v --update $fromDir/plugins  --target-directory=$toDir

fromDir=/home/data/httpd/download.eclipse.org/datatools/updates

cp -r -v --update $fromDir/features --target-directory=$toDir
cp -r -v --update $fromDir/plugins  --target-directory=$toDir

fromDir=/home/data/httpd/download.eclipse.org/tools/cdt/releases/3.1.0/

cp -r -v --update $fromDir/features --target-directory=$toDir
cp -r -v --update $fromDir/plugins  --target-directory=$toDir

fromDir=/home/data/httpd/download.eclipse.org/tools/ve/updates/1.0/development

cp -r -v --update $fromDir/features --target-directory=$toDir
cp -r -v --update $fromDir/plugins  --target-directory=$toDir

fromDir=/home/data/httpd/download.eclipse.org/webtools/milestones

cp -r -v --update $fromDir/features --target-directory=$toDir
cp -r -v --update $fromDir/plugins  --target-directory=$toDir


fromDir=/home/data/httpd/download.eclipse.org/tptp/updates/4.2milestones/4.2.0M5/

cp -r -v --update $fromDir/features --target-directory=$toDir
cp -r -v --update $fromDir/plugins  --target-directory=$toDir


#fromDir=/home/data/httpd/download.eclipse.org/birt/update-site
#
#cp -r -v --update $fromDir/features --target-directory=$toDir
#cp -r -v --update $fromDir/plugins  --target-directory=$toDir
#
#fromDir=/home/data/httpd/download.eclipse.org/technology/gmf/update-site
#
#cp -r -v --update $fromDir/features --target-directory=$toDir
#cp -r -v --update $fromDir/plugins  --target-directory=$toDir
