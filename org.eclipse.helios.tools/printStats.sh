      


if [ -z "${toDirectory}" ] 
then
    toDirectory=/home/data/httpd/download.eclipse.org/releases/maintenance  
fi  

echo "repository file statistics for ";
echo $toDirectory;

if [ ! -e "${toDirectory}" ]
then
  echo "repository directory does not exist. Exiting." 
  exit 1;
fi
  
featuresDirectory="${toDirectory}"/aggregate/features
pluginsDirectory="${toDirectory}"/aggregate/plugins

if [ ! -e "${featuresDirectory}" ]
then 
  echo "repository features directory structure not as expected. Exiting." 
  exit 2;
fi

if [ ! -e "${pluginsDirectory}" ]
then 
  echo "repository plugins directory structure not as expected. Exiting."
  exit 3;
fi
 
echo "Features: ";     
find "${featuresDirectory}" | wc -l  
du -sh "${featuresDirectory}"
 
echo "Plugin jar files: ";
find "${pluginsDirectory}" -name "*.jar" | wc -l 
du -sh --exclude=*.pack.gz "${pluginsDirectory}"

echo "Plugin pack.gz files: "; 
find "${pluginsDirectory}" -name "*jar.pack.gz" | wc -l
du -sh --exclude=*.jar "${pluginsDirectory}"


 