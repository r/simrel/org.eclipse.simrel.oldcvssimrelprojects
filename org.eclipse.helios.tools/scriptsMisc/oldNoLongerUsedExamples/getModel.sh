#!/usr/bin/env bash


# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source aggr_properties.shsource
 
echo "   removing all of previous project ..."
rm -fr ${BUILD_HOME}/org.eclipse.helios.build/*
mkdir -p ${BUILD_HOME}/org.eclipse.helios.build

buildBranch=HEAD

cd ${BUILD_HOME}
echo "   checking out branch of project from cvs ..."
cvs -d :pserver:anonymous@dev.eclipse.org:/cvsroot/callisto  export -d org.eclipse.helios.build -r $buildBranch org.eclipse.helios.build

