#!/usr/bin/env bash
# script to copy update jars from their working area to the staging area

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source aggr_properties.shsource
 
toDirectory=${stagingDirectory} 
#toDirectory=${releaseDirectory}

# obfuscate email addresses
fromString="mailto:(.*)@"
toString="mailto:"'$1'"<at>"
replaceCommand="s!$fromString!$toString!"
echo "replaceCommand: $replaceCommand"
pexec=`echo "perl -pi -w -e " $replaceCommand "${toDirectory}/index.php"`
echo "$pexec"
perl -pi -w -e $replaceCommand "${toDirectory}/index.php"


