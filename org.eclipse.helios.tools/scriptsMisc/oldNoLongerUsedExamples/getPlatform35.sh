#!/usr/bin/env bash
# Little utility to unzip Eclipse Platfrom into ${ECLIPSE_INSTALL_35}


# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source aggr_properties.shsource

if [ -z "${FULL_FILENAME_35}" ]
then 

       # Note that 'downloads home' could also be written as "${HOME}/downloads" for most users
       # but this form should work for all (unless 'downloads' sym link changes, which is unlikely)
       DOWNLOADS_HOME=/home/data/httpd/download.eclipse.org
       # DROP_DIR and DROP_ID must be changed to match desired version to fetch
       DROP_DIR=R-3.5.2-201002111343
       # DROP_ID can be different from DROP_DIR, such as for milestone's it'd
       # be similar to S-3.5M6, but for I-builds is the same.
       DROP_ID=3.5.2
       PLATFORM_FILENAME=eclipse-SDK-${DROP_ID}-linux-gtk.tar.gz
       FULL_FILENAME_35=${DOWNLOADS_HOME}/eclipse/downloads/drops/${DROP_DIR}/${PLATFORM_FILENAME}

fi 

if [ ! \( -e ${FULL_FILENAME_35} \) ]
then
    echo "Error. File did not exist: ${FULL_FILENAME_35}"
    exit 9;
fi

function installPlatform () 
{
    echo;
    echo "Installing "
    echo "  ${FULL_FILENAME_35}"
    echo "  into "
    echo "  ${ECLIPSE_HOME_35}"
    mkdir -p ${ECLIPSE_HOME_35}
    tar -zxf ${FULL_FILENAME_35} -C ${ECLIPSE_HOME_35}

}

# make sure we are completely fresh before unzipping
if [ -d ${ECLIPSE_HOME_35} ]
then

        echo "Removing existing version at "
        echo "   ${ECLIPSE_HOME_35}"
        rm -fr ${ECLIPSE_HOME_35}
fi

# now install
    installPlatform
