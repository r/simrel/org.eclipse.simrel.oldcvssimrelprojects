#!/usr/bin/env bash
# script to copy update jars from their working area to the staging area

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source aggr_properties.shsource
 
#TODO: now multiple to choose from?
#fromDirectory=${BUILD_RESULTS}
fromDirectory=${AGGREGATOR_RESULTS}

toDirectory=${stagingDirectory} 
# temp area for testing
#toDirectory=${HOME}/temp/testdir
tempDir=${HOME}/temp/work

# make sure 'toDirectory' has been defined and is no zero length, or 
# else following will eval to "rm -fr /*" ... potentially catastrophic
if [ -z "${toDirectory}" ]
then
	echo;
    echo "   Fatal Error: the variable toDirectory must be defined to run this script"
    echo;
else

       echo "";
       echo "    Count of old features, bundle jars, and packed jars prior to promotion";
       "${BUILD_TOOLS_DIR}"/printStats.sh
       echo "";

	echo "";
	echo "    Removing previous staging directory files at"
	echo "    "${toDirectory} 
	echo "";

	if [ -d ${toDirectory} ]
	then
	  rm -fr "${toDirectory}"/*
	fi
	
	echo ""
	echo "    Copying new plugins and features "
	echo "        from  ${fromDirectory}"
	echo "          to  ${toDirectory}"
	echo ""
	
	# plugins and features
	rsync -rvp ${fromDirectory}/final/aggregate ${toDirectory}
	
	# composite artifact and content files
	rsync -vp ${fromDirectory}/final/*.jar ${toDirectory}
	
	# static index page
	rsync -vp templateFiles/maintenance/index.html ${toDirectory}
	
	"${BUILD_TOOLS_DIR}"/addRepoProperties-staging.sh

       # TODO: eventually, we could make "sanity check" test on number of files, etc., and fail if more than 10% off, or similar
       echo "";
       echo "    Count of new features, bundle jars, and packed jars after promotion";
       "${BUILD_TOOLS_DIR}"/printStats.sh
       echo "";
	
fi

# remove lock file from hundson build's "pauseAll.sh" script once we are all done.
# remember, we need to _always_ remove the lock file, so do not "exit" from any previous script
rm -vf "${BUILD_HOME}"/lockfile
