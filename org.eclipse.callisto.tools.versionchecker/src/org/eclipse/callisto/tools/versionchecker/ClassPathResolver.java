/*******************************************************************************
 * Copyright (c) 2006 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *     
 *******************************************************************************/

package org.eclipse.callisto.tools.versionchecker;

import java.io.IOException;
import java.io.InputStream;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * ClassPathResolver
 */

public class ClassPathResolver implements EntityResolver {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xml.sax.EntityResolver#resolveEntity(java.lang.String,
	 *      java.lang.String)
	 */
	public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {

		String id = systemId;
		if (id.startsWith("file:///")) {
			id = id.substring("file:///".length()); 
		}
		InputStream stream = getClass().getResourceAsStream(id);
		if (stream == null) {
			return null;
		}
		else {
			return new InputSource(stream);
		}
	}

}
