@echo off

rem specify devworkspace and JRE to use to runEclipse

set devworkspace=%1
if %devworkspace%. == . set devworkspace=./workspace

rem this is a SUN 1.4.2 JRE
set devJRE=\JDKs\j2sdk1.4.2_11\bin\javaw.exe

rem IBM's 1.4.2 JRE
rem set devJRE=\JDKs\ibm-sdk-n142p-win32-x86\bin\java.exe

rem Sun's 1.5 JRE
rem set devJRE=\JDKs\jdk1.5.0_07\bin\java.exe
rem set devJRE=\JDKs\jdk1.5.0_05\bin\java.exe

rem IBM's 1.5 JRE
rem set devJRE=\JDKs\ibm-java2-sdk-50\bin\java.exe



echo dev:          %0
echo.
echo devworkspace: %devworkspace%
echo.
echo devJRE:       %devJRE%
%devJRE% -version
echo.

rem set versionListerCommands=-listToReferenceFile 
set versionListerCommands=-testToReference WTPSDK151

eclipse -application org.eclipse.callisto.tools.versionchecker.listVersions -clean -nosplash -showlocation -refresh  -data %devworkspace%  %versionListerCommands% -vm %devjre% -vmargs  -showversion -Xmx256M 1>versionCheckerOutput.txt

