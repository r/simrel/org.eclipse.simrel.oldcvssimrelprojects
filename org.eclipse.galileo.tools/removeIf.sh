#!/bin/bash

# echo "arg: $1";

if [ -z $1 ]
  then
    echo "  This script requires an argument. " ;
    exit 1;
  fi

foundDirectory=$1
# should already be in foundDirectory, if execDir used
cd $foundDirectory
# move up one so we can examine syblings
cd ..
currentDirectory=`pwd`
echo $currentDirectory
ndirs=`ls -lA | wc -l`
ndirs=$(($ndirs - 1)); # don't count the "totals" line
# echo "NDirs: $ndirs"

# if only one left, do not remove it, no matter how old
if [ $ndirs -eq 1 ]
then
    exit 0;
fi
# This is unexpected, since otherwise this method should not have been called. 
# So, this check is just a safety check.
if [ $ndirs -lt 1 ]
then
    exit 101;
fi

# ok, it is old, and not the only one left
# adding a few "should never happen" errror checks
if [ -d $foundDirectory ] 
then
  basedirname=$(basename $foundDirectory)
  if [[ "$basedirname" =~ "[[:digit:]]{14}" ]]
    then
       echo "removing $basedirname"
       rm -fr $foundDirectory
    else
     # patches is expected, warn of others
      if [ "patches" != $foundDirectory ]
       then 
         echo "ERROR: The foundDirectory, $foundDirectory, didn't match timestamp pattern"
       fi 
    fi
else
    echo "ERROR: the foundDirectory, $foundDirectory, is not a directory"
fi
