#!/usr/bin/env bash

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source galileo_properties.shsource
 
APP_NAME=org.eclipse.equinox.p2.director.app.application
OTHER_ARGS="-metadataRepository http://download.eclipse.org/tools/buckminster/tools-galileo/ -artifactRepository http://download.eclipse.org/tools/buckminster/tools-galileo/ -installIU org.eclipse.buckminster.galileo.builder.feature.feature.group"
# usually do not need the version, just to "get the latest" from buckminster builder site
# -version 1.0.0.v20090415-1651
VM_ARGS="-vmargs -Xmx256m"

"${ECLIPSE_HOME}"/eclipse -consolelog -data ./workspace-getBuckminsterBuildUpdate -debug -nosplash --launcher.suppressErrors -vm "${JAVA_EXEC_DIR}" -application ${APP_NAME} ${OTHER_ARGS} ${VM_ARGS}



