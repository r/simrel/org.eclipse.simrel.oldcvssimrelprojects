#!/usr/bin/env bash
# script to copy update jars from their staging area to the releases area

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source galileo_properties.shsource

source fixupMirrorURL.sh
 
fromDirectory=${stagingDirectory}
toDirectory=${releaseDirectory} 

datetimestamp=$1

# make sure 'toDirectory' has been defined and is not zero length
if [ -z "${toDirectory}" ]
then
	echo;
    echo "   Fatal Error: the variable toDirectory must be defined to run this script";
    echo;
else

	# make sure 'datetimestamp' has been defined and is no zero length
	if [ -z "${datetimestamp}" ]
	then
	   echo;
	   echo "   Fatal Error: the variable datetimestamp must be defined to run this script"
	   echo;
	else
	
		toSubDir=${toDirectory}/${datetimestamp}
		
		echo ""
		echo "    Copying new plugins and features "
		echo "        from  ${fromDirectory}"
		echo "          to  ${toSubDir}"
		echo ""
		
        # plugins and features
        rsync -rvp ${fromDirectory}/aggregate ${toSubDir}

        # composite artifact and content files
        rsync -vp ${fromDirectory}/*.jar ${toSubDir}

        # static index page
        rsync -vp templateFiles/release/index.html ${toDirectory}

		fixupMirrorURL ${toSubDir}/aggregate/artifacts.jar galileo/${datetimestamp}
	fi

fi

# remove lock file from hundson build's "pauseAll.sh" script once we are all done.
# remember, we need to _always_ remove the lock file, so do not "exit" from any previous script
rm -vf "${BUILD_HOME}"/lockfile

