#!/usr/bin/env bash
# Little utility to unzip Eclipse Platfrom into ${ECLIPSE_INSTALL}

# The first to variables will need to be set accurately for
# a particular release.

# DROP_DIR and DROP_ID must be changed to match desired version to fetch
DROP_DIR=R-3.5-200906111540

# DROP_ID can be different from DROP_DIR, such as for milestone's it'd
# be similar to S-3.5M6, but for I-builds is the same.
DROP_ID=3.5


# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source galileo_properties.shsource


# The following should not need to be changed ... well, hm, except the OS details, if running elsewhere

# Note that 'downloads home' could also be written as "${HOME}/downloads" for most users
# but this form should work for all (unless 'downloads' sym link changes, which is unlikely)
DOWNLOADS_HOME=/home/data/httpd/download.eclipse.org
PLATFORM_FILENAME=eclipse-SDK-${DROP_ID}-linux-gtk-ppc.tar.gz
FULL_FILENAME=${DOWNLOADS_HOME}/eclipse/downloads/drops/${DROP_DIR}/${PLATFORM_FILENAME}

# make sure we are completely fresh before unzipping
if [ -d ${ECLIPSE_HOME} ]
then
	# bug make sure we really can get our computed
	# filename before removing old one
	if [ -e ${FULL_FILENAME} ]
	then
        rm -fr ${ECLIPSE_HOME}
        tar -zvxf ${FULL_FILENAME} -C ${ECLIPSE_INSTALL}
	else
	    echo "Error. File did not exist: ${FULL_FILENAME}"
    fi
else
    # didn't previously exist, so just try to unzip
    mkdir -p ${ECLIPSE_INSTALL}
    tar -zvxf ${FULL_FILENAME} -C ${ECLIPSE_INSTALL}
fi



