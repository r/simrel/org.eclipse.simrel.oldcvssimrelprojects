#!/usr/bin/env bash
# script to fix mirror URL in an artifact.jar file

function fixupMirrorURL {

	artifactFilePath=$1
	
	# target segment is what ever comes beteeen 'releases/' and '/aggregate'. Typically, 
	# maintenance
	# galileo/<datetimestamp>
	# staging
	# helios/<datetimestamp>
	targetSegment=$2
	
	# make sure 'artifactFilePath' has been defined and is not zero length
	if [ -z "${artifactFilePath}" ]
	then
		echo;
	    echo "   Fatal Error: The variable 'artifactFilePath' must be defined as first argument to run this script"
	    echo;
	    return 1;
	fi
	
	# make sure 'artifactFilePath' exists
	if [ ! -f "${artifactFilePath}" ]
	then
	    echo;
	    echo "   Fatal Error: The file '${artifactFilePath}' does not exist."
	    echo;
	    return 2;
	fi
	
	# make sure 'targetSegment' has been defined and is not zero length
	if [ -z "${targetSegment}" ]
	then
		echo;
	    echo "   Fatal Error: The variable 'targetSegment' must be defined as first argument to run this script";
	    echo;
	    return 3;
	fi
	
	tempDir=${HOME}/temp/work
	mkdir -p "${tempDir}"
	# now correct the URLs
	fromFile=artifacts.jar
	workFile=artifacts.xml
	unzip ${artifactFilePath} -d ${tempDir}
	
	matchingPhrase="p2\.mirrorsURL.*/releases/.*/aggregate"
	matchedLines=`grep "${matchingPhrase}" "${tempDir}"/"$workFile"`
	nMatches=`echo $matchedLines | wc -l`
	echo
	echo "    matches before replacement: "${nMatches}
	echo "    matchedLine(s) before replacement: "
	echo "        "$matchedLines
	echo
	
	if [[ $nMatches == 1 ]]
	then
		 # note we make no restiction what is between p2.mirrorsURL and 'releases'
		 # nor between 'releases' and 'aggregate'. 
		 # our particular case allows us to "throw away" the later, but restore the former.
		 fromString="p2\.mirrorsURL(.*)/releases/.*/aggregate"
		
		 # compose toString and replacement command
		 toString="p2\.mirrorsURL\$1/releases/${targetSegment}/aggregate"
		 replaceCommand="s!${fromString}!${toString}!g"
		 echo "    replaceCommand: ${replaceCommand}"
		
		 perl -pi -w -e ${replaceCommand} "${tempDir}"/"${workFile}"
		
		 perlResult=$?
		 if [[ $perlResult -ne 0 ]] 
		 then 
		 	echo;
		    echo "   Fatal Error: Perl return code was non-zero, ${perlResult}, so returning and not changing original file.";
		    echo;
		    return $perlResult;
		 fi
		
		 resultingPhrase="p2\.mirrorsURL.*/releases/${targetSegment}/aggregate"
		 changedLines=`grep  "${resultingPhrase}" "${tempDir}"/"$workFile"`
		 nChangedLines=`echo $changedLines  | wc -l`
		 echo
		 echo "   Number of lines changed: " $nChangedLines
		 echo "   Changeded lines: " 
		 echo "      "$changedLines
		 echo
		 
		 if [[ $nChangedLines -ne 1 ]]
		 then
		 	echo;
		    echo "   Fatal Error: number of matches to expected ("$nChangedLines") after replacement was not as expected, so returning and not changing original file.";
		    echo;
		    return 10;
		 fi
	else
	    echo;
	    echo "   Fatal Error: number of matches to original ("$nMatches") before replacment was not as expected, so returning and not changing original file.";
	    echo;
	    return 11;
	fi
	
	echo;
	echo "   Rezipping the modified version and replacing original file";
	echo; 
	zip -Djm ${artifactFilePath} "${tempDir}"/"${workFile}"

}