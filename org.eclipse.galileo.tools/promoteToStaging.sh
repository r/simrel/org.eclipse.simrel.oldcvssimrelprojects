#!/usr/bin/env bash
# script to copy update jars from their working area to the staging area

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source galileo_properties.shsource
 
source fixupMirrorURL.sh
 
fromDirectory=${BUILD_RESULTS}

toDirectory=${stagingDirectory} 
# temp area for testing
#toDirectory=${HOME}/temp/testdir
tempDir=${HOME}/temp/work

# make sure 'toDirectory' has been defined and is no zero length, or 
# else following will eval to "rm -fr /*" ... potentially catastrophic
if [ -z "${toDirectory}" ]
then
	echo;
    echo "   Fatal Error: the variable toDirectory must be defined to run this script"
    echo;
else

	echo "";
	echo "    Removing previous staging directory files at"
	echo "    "${toDirectory} 
	echo "";

if [ -d ${toDirectory} ]
then
  rm -fr "${toDirectory}"/*
fi

echo ""
echo "    Copying new plugins and features "
echo "        from  ${fromDirectory}"
echo "          to  ${toDirectory}"
echo ""

# plugins and features
rsync -rvp ${fromDirectory}/final/aggregate ${toDirectory}

# composite artifact and content files
rsync -vp ${fromDirectory}/final/*.jar ${toDirectory}

# generic html index page for 'staging'
rsync -vp templateFiles/staging/index.html ${toDirectory}

fixupMirrorURL ${toDirectory}/aggregate/artifacts.jar maintenance
fi

# remove lock file from hundson build's "pauseAll.sh" script once we are all done.
# remember, we need to _always_ remove the lock file, so do not "exit" from any previous script
rm -vf "${BUILD_HOME}"/lockfile
