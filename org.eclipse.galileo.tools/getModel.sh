#!/usr/bin/env bash


# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source galileo_properties.shsource
 
echo "   removing all of previous project ..."
rm -fr ${BUILD_HOME}/org.eclipse.galileo.build/*
mkdir -p ${BUILD_HOME}/org.eclipse.galileo.build

buildBranch=HEAD

cd ${BUILD_HOME}
echo "   checking out branch of project from cvs ..."
cvs -d :pserver:anonymous@dev.eclipse.org:/cvsroot/callisto  export -d org.eclipse.galileo.build -r $buildBranch org.eclipse.galileo.build

