#!/usr/bin/env bash

# finds file on users path, before current directory
# hence, non-production users can set their own values for test machines
source galileo_properties.shsource
 
export BUILD_TIMESTAMP=$(date --utc +%Y%m%d%H%M%S); 
echo "BUILD_TIMESTAMP: " $BUILD_TIMESTAMP

export BUILD_ID=${BUILD_TIMESTAMP}

OTHER_ARGS="-mockEmailTo david_williams@us.ibm.com -buildModel ${BUILD_HOME}/org.eclipse.galileo.build/galileo.build -buildRoot ${BUILD_RESULTS} -buildId ${BUILD_ID} -logLevel DEBUG"

VM_ARGS="-vmargs -Declipse.p2.mirrors=false -Xmx256m"
# -Declipse.p2.MD5Check=false

APP_NAME=org.eclipse.buckminster.galileo.builder.app

"${ECLIPSE_HOME}"/eclipse -consolelog -data ${BUILD_HOME}/${BUILD_TIMESTAMP}/workspace-runBuckyBuild -debug -nosplash --launcher.suppressErrors -vm "${JAVA_EXEC_DIR}" -application ${APP_NAME} ${OTHER_ARGS} ${VM_ARGS}


