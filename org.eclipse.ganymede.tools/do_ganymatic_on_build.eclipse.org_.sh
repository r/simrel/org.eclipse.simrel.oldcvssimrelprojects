#
# This script is the specialization for running the Ganymatic on
# the build.eclipse.org machine for the daily Ganymede builds.
# It gets the latest code, sets up environment variables, etc.
# If you'd like to run the Ganymatic on your own machine, you'll
# do something similar, but for your own machine and environment.
# For example, when I run Ganymatic on my laptop, I run it from
# Eclipse, so I use Eclipse's CVS support to get the latest
# versions of the code; and I use Eclipse's Ant support to run
# the ant script; etc.
#
# This script should be "copied up" and run from the main
# directory (~/ganymede).
#
# Be sure to run "get-fresh-script" whenever 'tools' should be updated. 
#
echo "   Doing primary, basic update staging site for Ganymede"

# we simulate cctimestamp, since it is not passed to shell scripts, 
# but once we move to ant, then all the code would be the same
cctimestamp=$(date +"%Y%m%d%H%M%S")

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
# TODO: avoid this "relative" dependancy 
  configFile=./org.eclipse.ganymede.tools/ganymatic.shsource
fi 

echo "     Ganymatic configuration read from " $configFile
echo 
source $configFile


echo "     removing old sitecontributions"
rm -fr org.eclipse.ganymede.sitecontributions

if [ "true" == "$CLEAN_CACHE" ]
then
    echo "     removing cached files"
    rm -fr org.eclipse.dash.siteassembler/bin/site
fi 

if [ -z ${G_CVS_SITE_CONTRIBUTIONS_TAG} ]
then
   G_CVS_SITE_CONTRIBUTIONS_TAG=HEAD
fi 

echo "     checkout fresh ${G_CVS_SITE_CONTRIBUTIONS_TAG} versions of sitecontributions"

cvs -f -d ${G_CVS_INFO}/cvsroot/callisto checkout -r ${G_CVS_SITE_CONTRIBUTIONS_TAG} org.eclipse.ganymede.sitecontributions

chmod -R a+rx org.eclipse.ganymede.sitecontributions 

echo "cctimestamp: " $cctimestamp

(cd ${GANYMEDE_BUILD_HOME}/workingdir/org.eclipse.ganymede.tools; ${ANT_HOME}/bin/ant -Dcctimestamp=$cctimestamp)

# In some cases, some users may want to override defaults and provide their own location 
# or file name for for ganymatic.properties file. That could be done as in following line.
#(cd ${GANYMEDE_BUILD_HOME}/workingdir/org.eclipse.ganymede.tools; ${ANT_HOME}/bin/ant -Dcctimestamp=$cctimestamp -Dganymatic.properties.file=ganymatic.properties)

# this script's exit code is the same as ant's exit code
exitcode=$?

echo
echo "exitcode: ${exitcode}"

fqdn=$(hostname -f)

echo
echo "$(basename $0) running on ${fqdn} is complete"
echo "   logs are: ${GANYMATIC_WEBSITE_URL}"
echo "   update site is: ${GANYMEDE_STAGING}"
echo

exit ${exitcode}
