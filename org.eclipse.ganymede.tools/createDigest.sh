#!/bin/sh

right_now=$(date +"%x %r %Z")
echo "   Create the site digest. Started at: " $right_now
echo

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
  configFile=ganymatic.shsource
fi

echo "     Ganymatic configuration read from " $configFile
source $configFile
if [ -z $1 ]
then
    echo "     no argument provided, doing 'staging' run"
    jarlocation=${GANYMEDE_STAGING}
else
if [ "-r" == "$1" ]
then 
    echo "     -r specified, doing 'release' run"
    jarlocation=${GANYMEDE_RELEASE}
else
    echo "     ERROR: argument " $1 " not recognized." 
    exit 1
fi 
fi

if [ -z $jarlocation ]
then
    echo "     ERROR: site (repository) location was not specified"
    exit 1
fi 

echo "     site (repository) location: " $jarlocation
echo

${REMOTE_SSH_COMMAND} ${JAVA_HOME}/jre/bin/java -Dorg.eclipse.update.jarprocessor.pack200=${JAVA_HOME}/jre/bin  -jar ${ECLIPSE_HOME}/plugins/${ECLIPSE_LAUNCHER} -application org.eclipse.update.core.siteOptimizer -verbose -digestBuilder -digestOutputDir=${jarlocation} -siteXML=${jarlocation}/site.xml

# touch the site.xml file, so it appears "last modified" (or, just modified) as this helps
# some mirror strategies to know the site has changed (they use the site.xml file to test if mirror site is available and up-to-date).
${REMOTE_SSH_COMMAND} touch ${jarlocation}/site.xml

# the "site proxy" file is used to denote "a successful build has occurred", 
# so the once-per-day builds (for packing) know whether to run, or not. 
# It is set here, under the assumption we only create digests after successful build.
# It is a "proxy" to the site.xml file, since, I've learned, for Cruisecontrol to
# "watch" this file for a changed datetime stamp, the file must _always_ exist, 
# even before there has been one build. 
${REMOTE_SSH_COMMAND} touch /opt/public/ganymede/workingdir/org.eclipse.ganymede.tools/siteProxyFile.txt


right_now=$(date +"%x %r %Z")
echo " "
echo "Completed digest at " $right_now
echo " "
