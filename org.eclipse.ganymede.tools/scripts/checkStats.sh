#!/bin/sh

# This script is to print some summary statistics on features and plugins on an update site

nfeatures=$(find ./features/ -name "*.jar"  | wc -l)
nfeaturesPacked=$(find ./features/ -name "*.gz"  | wc -l)
nplugins=$(find ./plugins/ -name "*.jar"  | wc -l)
npluginsPacked=$(find ./plugins/ -name "*.gz"  | wc -l)
nfeaturesNotPacked=$(($nfeatures - $nfeaturesPacked))
echo
echo "  number of features: $nfeatures ($nfeaturesNotPacked are not packed)"
echo "  number of plugins: $nplugins ($(($nplugins-$npluginsPacked)) are not packed)"

find ./features/ -name "*.jar"  | sort | sed -e 's/\.jar$//' > features-jar.txt
find ./features/ -name "*.gz"  | sort | sed -e 's/\.jar\.pack\.gz$//'  > features-gz.txt
./listExtra.sh features-jar.txt features-gz.txt > non-packed-features.txt
echo
echo "  Non-packed features"
echo
cat non-packed-features.txt

find ./plugins/ -name "*.jar"  | sort | sed -e 's/\.jar$//' > plugins-jar.txt
find ./plugins/ -name "*.gz"  | sort  | sed -e 's/\.jar\.pack\.gz$//' > plugins-gz.txt
./listExtra.sh plugins-jar.txt plugins-gz.txt > non-packed-plugins.txt
echo
echo "  Non-packed plugins"
echo
cat non-packed-plugins.txt

