#!/bin/sh

# This script is to customize the output of 'diff' command, to be more human-readable 
# for this specific case of list lines in one file, that are not in another 

diff \
--old-line-format=' %l
' \
--new-line-format='> %l
' \
--old-group-format='%<' \
--new-group-format='%>' \
--changed-group-format='%df%(f=l?:,%dl)c%dF%(F=L?:,%dL)
%<---
%>' \
--unchanged-group-format='' \
$1 $2
