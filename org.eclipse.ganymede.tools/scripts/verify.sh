#!/bin/sh

# script to help beutify output from verifying a long list of jars in a directory

# echo
# echo $(basename $1
jarname=$(basename $1)

# purposely no line delimiter, so output of jarsigner is on same line
printf '%-100s \t\t' "   ${jarname}: "
/shared/webtools/apps/ibm-java2-sdk-5.0-6.0-linux-ppc/bin/jarsigner -verify $1
exitcode=$?

if [ $exitcode -gt 0 ]
then
    echo "\nexitcode: $exitcode: $(basename $1)"
fi


