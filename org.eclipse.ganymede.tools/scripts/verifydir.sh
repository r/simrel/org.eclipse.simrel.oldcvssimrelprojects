#!/bin/sh

# Script to run through all the jars in a directory to be sure signed (correctly)

if [ -z $1 ]
then
    loc=./
else
    loc=${1}
fi
if [ -z $2 ]
then
    pat="*.jar"
else
    pat="${2}"
fi

echo "verify directory: ${loc}";
echo "   for pattern:    ${pat}";

find "${loc}" -name "${pat}" -exec verify.sh '{}' \;

