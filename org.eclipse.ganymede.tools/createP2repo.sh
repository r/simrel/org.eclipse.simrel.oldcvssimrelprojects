#!/bin/sh

right_now=$(date +"%x %r %Z")
echo "   Create P2 repository. Started: " $right_now
echo

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
  configFile=ganymatic.shsource
fi

echo "     Ganymatic configuration read from " $configFile
source $configFile

if [ -z $1 ]
then
	echo "     no argument provided, doing 'staging' run"
	jarlocation=${GANYMEDE_STAGING}
else
if [ "-r" == "$1" ]
then 
	echo "     -r specified, doing 'release' run"
	jarlocation=${GANYMEDE_RELEASE}
else
    echo "     ERROR: argument " $1 " not recognized." 
    exit 1
fi 
fi

if [ -z $jarlocation ]
then
	echo "     ERROR: site (repository) location was not specified"
	exit 1
fi     

echo "     site (repository) location: " $jarlocation
echo

# we could probably use -append (in some cases), 
# but to make sure we get completely fresh metadata, 
# we'll re-generate from scratch. 
${REMOTE_SSH_COMMAND} rm -fv ${jarlocation}/content.jar
${REMOTE_SSH_COMMAND} rm -fv ${jarlocation}/artifacts.jar

${REMOTE_SSH_COMMAND} ${JAVA_HOME}/jre/bin/java -jar ${ECLIPSE_HOME}/plugins/${ECLIPSE_LAUNCHER} \
   -application "org.eclipse.equinox.p2.metadata.generator.EclipseGenerator" \
   -updateSite "${jarlocation}" \
   -site "file:${jarlocation}/site.xml" \
   -metadataRepository "file:${jarlocation}" \
   -metadataRepositoryName "Ganymede Update Site" \
   -artifactRepository "file:${jarlocation}" \
   -artifactRepositoryName "Ganymede Artifacts" \
   -compress \
   -reusePack200Files \
   -noDefaultIUs


# touch the site.xml file, so it appears "last modified" (or, just modified) as this helps
# some mirror strategies to know the site has changed (they use the site.xml file to test if mirror site is available and up-to-date).
${REMOTE_SSH_COMMAND} touch ${jarlocation}/site.xml

right_now=$(date +"%x %r %Z")
echo
echo "   Completed P2 creation" $right_now
echo
