#!/bin/sh

# script to automate the promotion of staging area to released area

# NOTE: this version cleans release area completely, 
# future versions may want to make incremental additions instead

./copyToRelease-2-clean-dest.sh
./copyToRelease-3-plugins.sh
./copyToRelease-4-sitefiles.sh
./createDigest.sh -r
./createP2repo.sh -r
