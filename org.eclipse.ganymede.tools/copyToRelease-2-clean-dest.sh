#!/bin/sh

# script to clean ganymede releases area

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
  configFile=ganymatic.shsource
fi

echo "     Ganymatic configuration read from " $configFile
source $configFile

toDir=/home/data/httpd/download.eclipse.org/releases/ganymede

${REMOTE_SSH_COMMAND} rm -rf $toDir/features
${REMOTE_SSH_COMMAND} rm -rf $toDir/plugins
${REMOTE_SSH_COMMAND} rm -f $toDir/site.xml
${REMOTE_SSH_COMMAND} rm -f $toDir/digest.zip
