package org.eclipse.ganymede.webgen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class GetStatusAsPropertyFile {

	public static void main(String[] args) throws IOException {
		GenerateStatusFile statusFile = new GenerateStatusFile(new File(args[0]));
		File fout = new File(args[1]);
		PrintWriter w = new PrintWriter(new FileWriter(fout));
		w.println("ganymatic.build.endtime=" + statusFile.end_time_dir);
		if( statusFile.keyword.equals("SUCCESS")) {
			w.println("ganymatic.build.successful=true");
		} else {
			w.println("ganymatic.build.error.emails=" + statusFile.email_addresses);
		}
		w.close();
	}

}
