package org.eclipse.ganymede.webgen;

import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetRecentTrimAsFile {
	final static int NUMBER_TO_KEEP = 15;
	public static void main(String[] args) throws IOException {
		File dir = new File(args[0]);
		final Pattern p1 = Pattern.compile("^\\d+_\\d+_\\d+_\\d+_\\d+$");
		String[] files = dir.list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				Matcher m1 = p1.matcher(name);
				if (m1.matches())
					return true;
				return false;
			}
		});
		Arrays.sort(files);
		File fout = new File(args[1]);
		PrintWriter w = new PrintWriter(new FileWriter(fout));
		if( files.length - NUMBER_TO_KEEP < 1 )
			w.println("bogusfilenamebecauseanemptylistdeletesallfiles.txt");
		for( int i = 0; i < files.length - NUMBER_TO_KEEP; i++ ) {
			w.println(files[i] + "/**/*");
		}
		w.close();
		fout = new File(args[2]);
		w = new PrintWriter(new FileWriter(fout));
		if( files.length - NUMBER_TO_KEEP < 1 )
			w.println("bogusfilenamebecauseanemptylistdeletesallfiles.txt");
		for( int i = 0; i < files.length - NUMBER_TO_KEEP; i++ ) {
			w.println(files[i] + "/");
		}
		w.close();
	}
}
