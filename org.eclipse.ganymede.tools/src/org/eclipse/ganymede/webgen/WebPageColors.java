package org.eclipse.ganymede.webgen;


public class WebPageColors {
	public static String success_color() {
		return "#CCFFCC";
	}

	public static String fail_color() {
		return "#FFCCCC";
	}

	public static String information_color() {
		return "#F4E0FF";
	}
	public static String inprogress_color() {
		return "#FFFFCC";
	}
}
