<?php
function get_status( $dir ) {
	$result = array();
	if( file_exists( "$dir/status.txt")) {
			$contents = file( "$dir/status.txt" );
			$result['statuscode'] = trim($contents[0]);
			$result['color'] = trim($contents[1]);
			$result['status'] = trim($contents[2]);
			$result['start-time'] = trim($contents[3]);
			$result['end-time'] = trim($contents[4]);
			// ignore $contents[5]
			// ignore bookmark $contents[6]
			// ignore emails $contents[7] 
			if( trim($result['end-time']) ) {
				$result['label'] = $result['end-time'];
				$result['logref1'] = "<a href=\"$dir/index.html\">";
				$result['logref2'] = "</a>";
			} else {
				$result['label'] = '<em>started ' . $result['start-time'] . '</em>';
				$result['logref1'] = '';
				$result['logref2'] = '';
			}
	} else {
		$result['statuscode'] = 'none';
		$result['color'] = 'white';
		$result['status'] = 'none';
		$result['label'] = '&nbsp;';
		$result['logref1'] = '';
		$result['logref2'] = '';
	}
	return $result;
}
$refreshstatus = get_status( 'latest' );
?>
<html>
<head>
<?php
if( $refreshstatus['statuscode'] == 'FAIL' ) {
	?><meta http-equiv="refresh" content="900"><!-- 15 minutes --><?php
} else if( $refreshstatus['statuscode'] == 'SUCCESS' ) {
	?><meta http-equiv="refresh" content="12000"><!-- ~4 hours --><?php
} else {
	?><meta http-equiv="refresh" content="300"><!-- 5 minutes --><?php
}
?>
<title>Ganymede Build Status</title>
<link rel="stylesheet" type="text/css" href="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/css/visual.css" media="screen" />
<link rel="alternate" type="application/rss+xml"
	title="Ganymede Build Status" href="builds.rss">
</head><body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr style="background-image: url(http://dash.eclipse.org/dash/commits/web-app/header_bg.gif);">
<td>
<a href="http://www.eclipse.org/"><img src="http://dash.eclipse.org/dash/commits/web-app/header_logo.gif" width="163" height="68" border="0" alt="Eclipse Logo" class="logo" /></a>
</td>
<td align="right" style="color: white; font-family: verdana,arial,helvetica; font-size: 1.25em; font-style: italic;"><b>Ganymede Build Status&nbsp;</b></font> </td>
</tr>
</table>

<table width=100% border=0><tr><td width=50% valign=top align="center">

<table align=center border=1><tr><th colspan=2>Latest Build</th></tr>
<?php
$status = get_status( 'latest' );
?><tr style="background-color: <?= $status['color'] ?>;">
<td><?= $status['logref1'] ?><?= $status['label'] ?><?= $status['logref2'] ?></td>
<td><?= $status['logref1'] ?><?= $status['status'] ?><?= $status['logref2'] ?></td>
</tr>
</table>
<p>
<table align=center border=1><tr><th colspan=2>Latest Good Build</th></tr>
<?php
$status = get_status( 'latest-good' );
?><tr style="background-color: <?= $status['color'] ?>;">
<td><?= $status['logref1'] ?><?= $status['label'] ?><?= $status['logref2'] ?></td>
<td><?= $status['logref1'] ?><?= $status['status'] ?><?= $status['logref2'] ?></td>
</tr>
</table>
<p>
<table align=center border=1>
<tr><td align=center> <a href="http://build.eclipse.org:9777/dashboard/tab/builds">Cruise Control</a> </td></tr>
<tr><td align=center> <a href="http://download.eclipse.org/releases/ganymede/staging/">Staging update site</a> </td></tr>
<tr><td align=center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="http://wiki.eclipse.org/Ganymede_Simultaneous_Release">Ganymede wiki page</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
<tr><td align=center> <table border=0><tr><td>
<a href="builds.rss"><img border="0" src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/images/rss_btn.gif"/> all builds</a> <br>
<a href="success.rss"><img border="0" src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/images/rss_btn.gif"/> successful builds</a> <br>
<a href="fail.rss"><img border="0" src="http://www.eclipse.org/eclipse.org-common/themes/Phoenix/images/rss_btn.gif"/> failed builds</a> 
	</td></tr></table></td></tr>
</table>
<p>
<table align=center border="1">
<tr><th colspan="5">Packages</th></tr>
<tr>
  <th>Build ID (UTC)</th>
  <th>CPP</th>
  <th>Java</th>
  <th>JEE</th>
  <th>RCP</th>
</tr>
<?php
$contents = file_get_contents( '/home/data/httpd/download.eclipse.org/technology/epp/downloads/testing/status.stub');
echo $contents;
?>
</table>

</td><td>

<table align=center border=1><tr><th colspan=2>Recent</th></tr>
<?php
$dirs = glob( "recent/*", GLOB_MARK | GLOB_ONLYDIR );
$dirs = array_reverse( $dirs );
foreach( $dirs as $dir ) {
	$dir = str_replace( '\\', '', $dir );
	if( $dir == '.' || $dir == '..' ) continue;
	$status = get_status( "$dir");
	?><tr style="background-color: <?= $status['color'] ?>;">
	<td><?= $status['logref1'] ?><?= $status['label'] ?><?= $status['logref2'] ?></td>
	<td><?= $status['logref1'] ?><?= $status['status'] ?><?= $status['logref2'] ?></td>
	</tr>
	<?php
}
if(count($dirs) == 0) {
	?><tr><td>&nbsp;</td><td>none</td></tr>
	<?php
}
?>
</table>

</td></tr>
</table>

<div style="margin-right: 50px; margin-top: 10px; 
float: right; text-align: right">
<table border="0" width="125" height="36"><tr><td align="right" valign="top" style="background-image: url(buck_logo_web_32.png)">
<font size="-1"><a href="http://www.eclipse.org/buckminster/"><em>powered by</em></a></font>
</td></tr></table>
</div>
</body>
</html>
