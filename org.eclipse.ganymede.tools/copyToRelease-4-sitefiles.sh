#!/bin/sh

# script to copy ganymede update jars from their staging to releases area

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
  configFile=ganymatic.shsource
fi

echo "     Ganymatic configuration read from " $configFile
source $configFile

toDir=/home/data/httpd/download.eclipse.org/releases/ganymede
fromDir=/home/data/httpd/download.eclipse.org/releases/ganymede/staging

${REMOTE_SSH_COMMAND} cp -f -p -r -v --update $fromDir/*html --target-directory=$toDir/
${REMOTE_SSH_COMMAND} cp -f -p -r -v --update $fromDir/*.gif  --target-directory=$toDir/
${REMOTE_SSH_COMMAND} cp -f -p -r -v --update $fromDir/site.xml --target-directory=$toDir/
${REMOTE_SSH_COMMAND} cp -f -p -r -v --update $fromDir/artifacts.jar --target-directory=$toDir/
${REMOTE_SSH_COMMAND} cp -f -p -r -v --update $fromDir/content.jar --target-directory=$toDir/

  ${REMOTE_SSH_COMMAND} perl -pi -e 's!ganymede/staging!ganymede!g' ${toDir}/site.xml
# ${REMOTE_SSH_COMMAND} perl -pi -e 's!ganymede/staging!ganymede!g' ${toDir}/index.html

# important to make sure digest is updated same time as site.xml's
# though typically recreated in subsequent step
${REMOTE_SSH_COMMAND} cp -f -p -r -v --update $fromDir/digest.zip --target-directory=$toDir/


# touch the site.xml file, so it appears "last modified" (or, just modified) as this helps
# some mirror strategies to know the site has changed (they use the site.xml file to test if mirror site is available and up-to-date).
${REMOTE_SSH_COMMAND} touch ${toDir}/site.xml
