#!/bin/sh

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
# TODO: avoid this "relative" dependancy 
  configFile=./org.eclipse.ganymede.tools/ganymatic.shsource
fi 

# in some cases, neither of the above files will exist, 
# so we have to trust defaults (which are, usually, correct)
if [ -e "$configFile" ]
then
  echo
  echo "   Ganymatic configuration read from " $configFile
  source "$configFile"
else
  echo "   No config file found ... assuming HEAD"
  G_CVS_BRANCH=HEAD
fi

echo
echo "   Get fresh script for Ganymatic ..."
echo "     removing old org.eclipse.ganymede.tools directory ..."

rm -fr org.eclipse.ganymede.tools

echo "     checkout fresh ${G_CVS_BRANCH} version of tools directory ..."

cvs -Q -f -d ${G_CVS_INFO}/cvsroot/callisto checkout -r ${G_CVS_BRANCH} org.eclipse.ganymede.tools

echo "     copy up to current directory ..."
# TODO avoid "copy up" of self (get-fresh-script.sh)
cp org.eclipse.ganymede.tools/do_ganymatic_on_build.eclipse.org_.sh .
cp org.eclipse.ganymede.tools/get-fresh-script.sh .

echo "     making sure files are executable and have proper EOL ..."
dos2unix *.sh* *.properties *.xml >/dev/null 2>>/dev/null
chmod +x *.sh > /dev/null
dos2unix org.eclipse.ganymede.tools/*.sh* *.properties *.xml >/dev/null 2>>/dev/null
chmod -R +x org.eclipse.ganymede.tools/*.sh > /dev/null
echo 
