#!/bin/sh

echo
right_now=$(date +"%x %r %Z")
echo "   Create pack200 files. Started: " $right_now
echo

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
  configFile=ganymatic.shsource
fi 

echo "     Ganymatic configuration read from " $configFile
source $configFile
if [ -z $1 ]
then
    echo "     no argument provided, doing 'staging' run"
    jarlocation=${GANYMEDE_STAGING}
else
if [ "-r" == "$1" ]
then 
    echo "     -r specified, doing 'release' run"
    jarlocation=${GANYMEDE_RELEASE}
else
    echo "     ERROR: argument " $1 " not recognized." 
    exit 1
fi 
fi

if [ -z $jarlocation ]
then
    echo "     ERROR: site (repository) location was not specified"
    exit 1
fi 

echo "     site (repository) location: " $jarlocation
echo


${REMOTE_SSH_COMMAND} ${JAVA_HOME}/jre/bin/java -Dorg.eclipse.update.jarprocessor.pack200=${JAVA_HOME}/jre/bin  -jar ${ECLIPSE_HOME}/plugins/${ECLIPSE_LAUNCHER} -application org.eclipse.update.core.siteOptimizer -jarProcessor -pack -verbose -outputDir ${jarlocation} ${jarlocation}

echo
echo "     remove zero length files introduced by the siteOptimizer (or, bad jars)"
echo

${REMOTE_SSH_COMMAND}  ${ANT_HOME}/bin/ant -f ${GANYMEDE_BUILD_HOME}/workingdir/org.eclipse.ganymede.tools/packStaging-2.xml -DupdateSite=${jarlocation}

echo
echo "   Completed Pack200"
echo
