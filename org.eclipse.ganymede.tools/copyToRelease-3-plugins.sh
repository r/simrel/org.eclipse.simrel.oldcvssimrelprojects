#!/bin/sh

# script to copy ganymede update jars from their staging to releases area

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
  configFile=ganymatic.shsource
fi

echo "     Ganymatic configuration read from " $configFile
source $configFile

toDir=/home/data/httpd/download.eclipse.org/releases/ganymede
fromDir=/home/data/httpd/download.eclipse.org/releases/ganymede/staging

${REMOTE_SSH_COMMAND} cp -f -p -r -v --update $fromDir/features --target-directory=$toDir/
${REMOTE_SSH_COMMAND} cp -f -p -r -v --update $fromDir/plugins  --target-directory=$toDir/

