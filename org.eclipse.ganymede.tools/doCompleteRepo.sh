#!/bin/sh


# Not really the complete repo ... just the time consuming part.
# assumes the basic site has already been created
# by do_ganymatic_on_build.eclipse.org_.sh.

echo "  Create pack200 files and P2 repository"

right_now=$(date +"%x %r %Z")
echo "   Started: " $right_now
echo

if [ -e ${HOME}/ganymedeConfig/ganymatic.shsource ]
then
  configFile=${HOME}/ganymedeConfig/ganymatic.shsource
else
  configFile=ganymatic.shsource
fi


echo "     Ganymatic configuration read from " $configFile
source $configFile
if [ -z $1 ]
then
    echo "     no argument provided, doing 'staging' run"
    jarlocation=${GANYMEDE_STAGING}
else
if [ "-r" == "$1" ]
then 
    echo "     -r specified, doing 'release' run"
    jarlocation=${GANYMEDE_RELEASE}
else
    echo "     ERROR: argument " $1 " not recognized." 
    exit 1
fi 
fi

if [ -z $jarlocation ]
then
    echo "     ERROR: site (repository) location was not specified"
    exit 1
fi 

echo "     site (repository) location: " $jarlocation
echo

${GANYMEDE_BUILD_HOME}/workingdir/org.eclipse.ganymede.tools/createPack200Files.sh
${GANYMEDE_BUILD_HOME}/workingdir/org.eclipse.ganymede.tools/createP2repo.sh

# touch the site.xml file, so it appears "last modified" (or, just modified) as this helps
# some mirror strategies to know the site has changed (they use the site.xml file to test if mirror site is available and up-to-date).
touch ${jarlocation}/site.xml

right_now=$(date +"%x %r %Z")
echo
echo "   Completed pack200 and P2 creation: " $right_now
echo
