package org.eclipse.indigo.tests.jars;

public abstract class TestJars {


    protected String bundleDirectory = null;
    private String   directoryToCheck;
    private String   tempWorkingDir;
    private String   outputDirectory;

    public String getDirectoryToCheck() {
        return directoryToCheck;
    }

    public void setDirectoryToCheck(String bundleDirToCheck) {
        this.directoryToCheck = bundleDirToCheck;
    }

    public String getOutputDirectory() {
        if (outputDirectory == null) {
            outputDirectory = System.getProperty("user.dir");
        }
        return outputDirectory;
    }

    public String getTempWorkingDir() {
        return tempWorkingDir;
    }

    public void setTempWorkingDir(String tempWorkingDir) {
        this.tempWorkingDir = tempWorkingDir;
    }

    public void setOutputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

}
