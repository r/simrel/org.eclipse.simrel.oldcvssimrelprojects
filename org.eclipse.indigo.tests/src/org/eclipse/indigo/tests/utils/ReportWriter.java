package org.eclipse.indigo.tests.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class ReportWriter {
    public ReportWriter(String outputDirectory, String outputFilename) {
        super();
        this.outputDirectory = outputDirectory;
        this.outputFilename = outputFilename;
    }

    private static final String EOL = System.getProperty("line.separator", "\n");
    private String              outputFilename;
    private String              outputDirectory;

    private PrintWriter         outfilewriter;

    public String getOutputFilename() {
        return outputFilename;
    }

    public void setOutputFilename(String outputFilename) {
        this.outputFilename = outputFilename;
    }

    public String getOutputDirectory() {
        return outputDirectory;
    }

    public void setOutputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }

    private PrintWriter getWriter() throws FileNotFoundException {
        if (outfilewriter == null) {
            File outfile = new File(getOutputDirectory(), getOutputFilename());
            outfilewriter = new PrintWriter(outfile);
        }
        return outfilewriter;
    }

    public void writeln(String text) throws IOException {
        getWriter().write(text + EOL);
    }

    public void writeln() throws IOException {
        getWriter().write(EOL);
    }

    public void close() {
        if (outfilewriter != null) {
            outfilewriter.close();
        }
    }

    public void writeln(Object object) throws IOException {
        writeln(object.toString());
    }

    public void printf(String format, Object... args) throws IOException {
        getWriter().printf(format, args);
    }
}
