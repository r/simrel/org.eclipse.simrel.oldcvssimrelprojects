/*******************************************************************************
 * Copyright (c) 2010 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.indigo.tests.repos;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.Set;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.apache.tools.ant.BuildException;
import org.eclipse.equinox.internal.p2.core.helpers.ServiceHelper;
import org.eclipse.equinox.p2.core.IProvisioningAgent;
import org.eclipse.equinox.p2.core.ProvisionException;
import org.eclipse.equinox.p2.metadata.IInstallableUnit;
import org.eclipse.equinox.p2.query.IQueryResult;
import org.eclipse.equinox.p2.query.QueryUtil;
import org.eclipse.equinox.p2.repository.metadata.IMetadataRepository;
import org.eclipse.equinox.p2.repository.metadata.IMetadataRepositoryManager;
import org.eclipse.indigo.tests.TestActivator;

/**
 * Tests that licenses in the repository are consistent with the platform
 * feature license.
 * 
 * This was based on test code originally attached to bug 306627
 * https://bugs.eclipse.org/bugs/show_bug.cgi?id=306627
 */
public class TestRepo extends TestCase {
    protected static final boolean DEBUG               = false;
    protected static final String  EOL                 = System.getProperty("line.separator", "\n");
    protected static final String  NBSP                = " &nbsp; ";
    protected static final String  BR                  = "<br/>" + EOL;
    private static final String    JUNIT_REPORT_OUTPUT = "junit-report-output";

    public static Test suite() {
        System.out.println("suite for layout");
        return new TestSuite(TestRepo.class);
    }


    private String repoURLToTest;
    private String outputDirectory;

    protected void println(FileWriter out, String wholeLine) throws IOException {
        out.write("<li>" + wholeLine + "</li>" + EOL);

    }

    protected boolean isSpecial(IInstallableUnit iu) {


        // TODO: I assume 'executable roots', etc. have no readable name? 
        /*
         * TODO: what are these special things? What ever they
         * are, they have no provider name. config.a.jre is
         * identified as a fragment
         * (org.eclipse.equinox.p2.type.fragment). a.jre has no
         * properties.
         */
        String iuId = iu.getId();
        boolean isSpecial = iuId.startsWith("a.jre") || iuId.startsWith("config.a.jre") || iuId.startsWith("org.eclipse.equinox.executable_root") || iuId.startsWith("org.eclipse.rcp.configuration_root") || iuId.startsWith("toolingorg.eclipse") || iuId.startsWith("tooling.");
        return isSpecial;
    }

    protected boolean isFeatureGroup(IInstallableUnit iu) {

        String iuId = iu.getId();
        boolean isFeatureGroup = iuId.endsWith("feature.group");
        return isFeatureGroup;
    }



    protected void printLineListItem(FileWriter outfileWriter, IInstallableUnit iu, String iuproperty) throws IOException {
        String iupropertyValue = iu.getProperty(iuproperty, null);
        String iuId = iu.getId();
        String iuVersion = iu.getVersion().toString();
        println(outfileWriter, iuId + NBSP + iuVersion + NBSP + BR + iupropertyValue);
    }

    /**
     * Use for debugging and exploration
     * 
     * @param outFileWriter
     * @param iu
     * @throws IOException
     */
    protected void printAllProperties(FileWriter outFileWriter, IInstallableUnit iu) throws IOException {
        Map<String, String> properties = iu.getProperties();
        Set keys = properties.keySet();
        for (Object key : keys) {
            String value = properties.get(key);
            println(outFileWriter, key + " : " + value);
        }

    }

    protected IQueryResult<IInstallableUnit> getAllIUs() throws URISyntaxException, ProvisionException {
        String repoURL = getRepoURLToTest();
        URI repoLocation = new URI(repoURL);
        IMetadataRepository repo = getMetadataRepositoryManager().loadRepository(repoLocation, null);
        assertNotNull("no repository found at " + repoLocation.toString(), repo);
        IQueryResult<IInstallableUnit> allIUs = repo.query(QueryUtil.createIUAnyQuery(), null);
        assertFalse(allIUs.isEmpty());
        return allIUs;
    }

    protected IQueryResult<IInstallableUnit> getAllGroupIUs() throws URISyntaxException, ProvisionException {
        String repoURL = getRepoURLToTest();
        URI repoLocation = new URI(repoURL);
        IMetadataRepository repo = getMetadataRepositoryManager().loadRepository(repoLocation, null);
        assertNotNull("no repository found at " + repoLocation.toString(), repo);
        IQueryResult<IInstallableUnit> allIUs = repo.query(QueryUtil.createIUGroupQuery(), null);
        assertFalse(allIUs.isEmpty());
        return allIUs;
    }

    protected static IMetadataRepositoryManager getMetadataRepositoryManager() {
        return (IMetadataRepositoryManager) getAgent().getService(IMetadataRepositoryManager.SERVICE_NAME);
    }

    protected static IProvisioningAgent getAgent() {
        // get the global agent for the currently running system
        return (IProvisioningAgent) ServiceHelper.getService(TestActivator.getContext(), IProvisioningAgent.SERVICE_NAME);
    }

    public String getRepoURLToTest() {
        if (repoURLToTest == null) {
            repoURLToTest = System.getProperty("repoURLToTest");

            if (repoURLToTest == null) {
                throw new BuildException("the 'repoURLToTest' property was not set");
            }

        }
        System.out.println("repoURLToTest: " + repoURLToTest);
        return repoURLToTest;
    }

    public void setRepoURLToTest(String repoURLToTest) {
        this.repoURLToTest = repoURLToTest;
    }

    public String getOutputDirectory() {
        if (outputDirectory == null) {
            outputDirectory = System.getProperty(JUNIT_REPORT_OUTPUT);
        }
        return outputDirectory;
    }

    public void setOutputDirectory(String outputDirectory) {
        this.outputDirectory = outputDirectory;
    }
}
